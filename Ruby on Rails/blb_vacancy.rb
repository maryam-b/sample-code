class Vacancy::BlbVacancy < Vacancy
  include Memoizer

  END_POINT = ProviderConstants::BLBConstants::API_URL
  NAMESPACE = ProviderConstants::BLBConstants::NAMESPACE
  USER = ProviderConstants::BLBConstants::USER
  PASSWORD = ProviderConstants::BLBConstants::PASSWORD

  TIMEOUT = 7

  LOGGING = true
  LOGFILE_PATH = 'log/savon-blb.log'



  def passenger_price_in_provider_currency_in_cents
    (ticket_price_list.total(pax_by_types) * 100).round
  end

  def vacancy_known?
    vacancy_request_successful? && price_request_successful?
  end

  def vacant?
    vacancy_known? && enough_seats_available?
  end

  private

  def vacancy_request_successful?
    requested_and_vacant_pax_by_types.select do |ticket_type_id, hash|
      hash[:vacant_pax].nil?
    end.empty?
  end

  def enough_seats_available?
    requested_and_vacant_pax_by_types.select do |ticket_type_id, hash|
      hash[:requested_pax] > hash[:vacant_pax]
    end.empty?
  end

  def price_request_successful?
    Integer(get_prices[:get_prices_response][:items]) > 0 if !get_prices[:get_prices_response][:prices].nil?
  end

  def requested_and_vacant_pax_by_types
    requested_and_vacant_pax_by_types = {}
    ticket_type_ids = pax_by_types.map { |pax_by_type| pax_by_type[:pax] > 0 ? pax_by_type[:ticket_type_id] : nil }.compact

    connection.provider.ticket_types.where(id: ticket_type_ids).each do |ticket_type|
      requested_pax = pax_by_types.select { |pax_by_type| pax_by_type[:ticket_type_id] == ticket_type.id }.first[:pax]

      requested_and_vacant_pax_by_types[ticket_type.id] = {
                                                requested_pax: requested_pax,
                                                vacant_pax: vacancy_request!(ticket_type.code)
                                              }
    end
    requested_and_vacant_pax_by_types
  end
  memoize :requested_and_vacant_pax_by_types

  def ticket_type_code_price_map
    code_price_map = {}
    if get_prices
      ticket_type_prices.each do |ticket_type_price|
        code_price_map[ticket_type_price[:code]] = ticket_type_price[:single].to_f
      end
    end
    code_price_map
  end

  def ticket_type_prices
    ticket_type_prices = get_prices[:get_prices_response][:prices][:item]
    # When BLB returns only one ticket type
    ticket_type_prices = [ticket_type_prices] unless ticket_type_prices.is_a?(Array)
    ticket_type_prices
  end

  def vacancy_request!(ticket_type_code)
    response = Retryable.retryable(tries: 4, sleep: lambda { |n| 2**n }, on: Savon::HTTPError) do
      savon_client.call(:create_ticket, vacancy_params(ticket_type_code)).to_hash
    end
    begin
       vacant_pax = Integer(response[:create_ticket_response][:returncode])
       vacant_pax < 0 ? (raise Booking::ProviderRemoteError::UnexpectedResponseError) : vacant_pax
    rescue ArgumentError, NoMethodError
    # vacancy unknown (either cannot convert to Integer or [:create_ticket_response][:returncode] does not exist)
      nil
    end

  rescue Savon::HTTPError => error
    raise Booking::ProviderRemoteError::ServerUnreachableError

  rescue Savon::SOAPFault, Savon::InvalidResponseError => error
    raise Booking::ProviderRemoteError::UnexpectedResponseError
  end

  def get_prices
      get_prices = Retryable.retryable(tries: 4, sleep: lambda { |n| 2**n }, on: Savon::HTTPError) do
        savon_client.call(:get_prices, get_prices_params).to_hash
      end

    return_code = begin
      Integer(get_prices[:get_prices_response][:items])
    rescue ArgumentError, NoMethodError
      nil
    end
    if return_code.nil? || return_code < 0 || ( return_code > 0 && get_prices[:get_prices_response][:prices][:item].nil? )
      raise Booking::ProviderRemoteError::UnexpectedResponseError
    end

    get_prices

  rescue Savon::HTTPError => error
    raise Booking::ProviderRemoteError::ServerUnreachableError

  rescue Savon::SOAPFault, Savon::InvalidResponseError => error
    raise Booking::ProviderRemoteError::UnexpectedResponseError
  end

  memoize :get_prices

  def vacancy_params(ticket_type_code)
    {
      message: {
        user: USER,
        password: PASSWORD,
        fwrd: {
          from: departure_place_code.code,
          dest: arrival_place_code.code,
          date: connection.departure_time.to_s(:db_with_utc_offset)},
          tarif: ticket_type_code,
          count: 0
        }
    }
  end

 def get_prices_params
    {
      message: {
        user: USER,
        password: PASSWORD,
        from: departure_place_code.code,
        dest: arrival_place_code.code,
        date: connection.departure_time.to_s(:db_with_utc_offset)
          }
    }
  end

  def departure_place_code
    connection.provider.place_code_for(connection.departure_place)
  end

  def arrival_place_code
    connection.provider.place_code_for(connection.arrival_place)
  end

  def savon_client
    if LOGGING
      savon_client_with_logging
    else
      savon_client_without_logging
    end
  end

  def endpoint
    END_POINT
  end

  def namespace
    NAMESPACE
  end

  def savon_client_with_logging
    Savon::Client.new(
        logger: logger,
        log: true,
        endpoint: endpoint,
        namespace: namespace,
        pretty_print_xml: true,
        open_timeout: TIMEOUT,
        read_timeout: TIMEOUT
    )
  end

  def savon_client_without_logging
    Savon::Client.new(
        endpoint: endpoint,
        namespace: namespace,
        open_timeout: TIMEOUT,
        read_timeout: TIMEOUT
    )
  end

  def logger
    logger = Logger.new(File.join(Rails.root, LOGFILE_PATH))
    logger.level = Logger::DEBUG
    logger
  end
end
