Feature: Select CHF as a Customer Currency

  @javascript
  Scenario: A user choose CHF currency and conducts a booking
    Given I can request information for a new booking
    When I am on the booking form for a connection
    Then the preselected currency is EUR

    When I select "CHF" from "booking[customer_currency]"
      # will be invalid
     And I press "Ticket jetzt kaufen"
    Then the customer_currency should be CHF

    # fill with valid stuff
    Given a request to bookings create will successfully create a booking in currency "CHF"
      And I can retrieve data for the booking in currency "CHF"
    When I submit the booking with valid customer details
    Then I should see the booking confirmation page
      And I should see "Vielen Dank"
      And I should see "CHF"
      But I should not see "EUR"