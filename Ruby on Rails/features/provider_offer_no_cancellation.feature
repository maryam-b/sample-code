Feature: Provider offer no cancellation

  @javascript
  Scenario: Provider doesn't offer cancellation
    Given I can perform agent login in core API
      And I can fetch affiliate credentials from core API
      And I can fetch connections index form data
    When I go to the homepage
    When I fill in "session_email" with "agency@example.com"
      And I fill in "session_password" with "password"
      And I press "Log in"
    Then I should be on the root page
      And I can fetch arrival station names scoped by departure station via ajax
      And I can fetch departure dates scoped by departure station and arrival station via ajax
      And I can fetch connections
      And I select "Nürnberg Hbf" from autocomplete field "Departure station" after typing "nu"
      And I select "Berlin Hbf" from autocomplete field "Arrival station" after typing "b"
      And I select tomorrow from datepicker
      And I click on "Search" within ".search"

    # bookings#new
    Given I can request information for a new booking provider offer no cancellation
    When I follow "Book now"
    Then I should see "* Mandatory field"
      And I should see "Title"
      And I should see "Mr"
      And I should see "Mrs"
      And I should see "First name"
      And I should see "Surname"
      And I should see "Street and house number"
      And I should see "Postcode"
      And I should see "City"
      And I should see "Email address"
      And I should see "Phone number"
      And I should see "Payment"
      And I should see "Amount will be debited from bank account set in agency account"
      And I should not see "IBAN"
      And I should not see "BIC"
      And I should have hidden input "booking_iban" filled with "DE12500105170648489890"
      And I should have hidden input "booking_bic" filled with "BENEDEPPYYY"
      And I should see "Passengers" within ".pasengers"
      And I should see "No." within ".passengers"
      And I should see "First name of passenger" within ".passengers"
      And I should see "Surname of passenger" within ".passengers"
      And I should see "Ticket type" within ".passengers"
      And I should see "Apply booking person as first passenger" within ".passengers"
      And I should see "Your booking"
      And I should see "Duration: 1 h 0 min"
      And I should see "Nürnberg Hbf"
      And I should see "Bahnhofsplatz 5, 90402 Nürnberg"
      And I should see "Departure: 09:00"
      And I should see "Berlin Hbf"
      And I should see "Europaplatz 1, 10557 Berlin"
      And I should see "Arrival: 10:00"
      And I should see "Operated by"
      And "Imprint" should link to "#" within ".booking-sidebar"
      And I should not see "Cancellation fee"
      And I should not see "This connection can be cancelled up to 48 hours before its departure. The cancellation fee is 6,50 €."
      And I should see "Total price"
      And I should see "incl. VAT"
      And I should see "Accept Terms & Conditions"
      And I should not see "Price per person"
      And I should have "search & book" navigation section active

    # bookings#create
    Given the paymill api returns a valid token
      And I can create booking provider offer no cancellation
      And I can fetch booking provider offer no cancellation
    When I choose "booking_form_of_address_mr"
      And I fill in "First name" with "Chris"
      And I fill in "Surname" with "Hansen"
      And I fill in "Street and house number" with "Foostrasse 42"
      And I fill in "Postcode" with "86150"
      And I fill in "City" with "Augsburg"
      And I fill in "Email address" with "foo@bar.de"
      And I fill in "Phone number" with "0160 123456"
      And I select "normal" from "ticket_type_id_0" within ".passenger:nth-of-type(1)"
      And I check "Accept Terms & Conditions"
      And I click on "Book Ticket Now"
    Then I should not see "A system error occured."
      And I should not see "An error occured during the transaction processing."
      And I should not see "IBAN is incorrect"

    # bookings#show (redirect after successful create)
    Then I should see "Now print ticket for your customer"
      And I should see "Booking" within "h1"
      And I should see "Booking number"
      And I should see "7KGDU8"
      And I should see "Booked at"
      And I should see booked at timestamp
      And I should see "Title"
      And I should see "Mr"
      And I should see "First name" within "dt:nth-of-type(4)"
      And I should see "Chris" within "dd:nth-of-type(4)"
      And I should see "Surname" within "dt:nth-of-type(5)"
      And I should see "Hansen" within "dd:nth-of-type(5)"
      And I should see "Street and house number"
      And I should see "Foostrasse 42"
      And I should see "Postcode"
      And I should see "86150"
      And I should see "City"
      And I should see "Augsburg"
      And I should see "Email address"
      And I should see "foo@bar.de"
      And I should see "Phone"
      And I should see "160123456"
      And I should see "Departure"
      And I should see "Nürnberg Hbf"
      And I should see "Departure address"
      And I should see "Bahnhofsplatz 5, 90402 Nürnberg"
      And I should see "Departure time"
      And I should see booking departure time
      And I should see "Destination"
      And I should see "Berlin Hbf"
      And I should see "Destination address"
      And I should see "Europaplatz 1, 10557 Berlin"
      And I should see "Arrival time"
      And I should see booking arrival time
      And I should see "Provider"
      And I should see "Evers-Crosskofp"
      And I should see "Provider booking number"
      And I should see "–"
      And I should see "legitimation number"
      And I should see "81 773"
      And I should see "Number of passengers"
      And I should see "1"

      And I should see "passengers"
      And I should see "Surname of passenger" within "table"
      And I should see "First name of passenger" within "table"
      And I should see "Ticket type" within "table"
      And I should see "Ticket price" within "table"
      And I should see "Hansen" within "table"
      And I should see "Chris" within "table"
      And I should see "normal" within "table"
      And I should see "8,50 EUR" within "table"

      And I should see "Total price"
      And I should see "8,50 EUR" within "dd:nth-of-type(22)"
      And I should see "Payment method"
      And I should see "sepa_direct_debit"

      And I should see "Cancellation"
      And I should not see "Not Cancelled"
      And I should see "This ticket is not refundable. For questions or amendments regarding your booking call customer service on +41 (0) 12 345 67 99."

      And I should have "Bookings" navigation section active