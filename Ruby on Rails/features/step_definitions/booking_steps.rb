Given(/^I can request information for a new booking( provider offer no cancellation| when paying by demand note| provider charges booking fee| provider cancellation is per ticket| in GMT| with fractional cancellation_cutoff_hours)?$/) do |booking_option|
  options = {}
  if booking_option
    cassette_name_mapping = {
      ' provider offer no cancellation' => 'fetch_api_reseller_bookings_new_offer_no_cancellation',
      ' when paying by demand note' => 'fetch_api_reseller_bookings_new_by_agency_paying_by_demand_note',
      ' provider charges booking fee' => 'fetch_api_reseller_bookings_new_charge_booking_fee',
      ' provider cancellation is per ticket' => 'fetch_api_reseller_bookings_new_cancellation_per_ticket',
      ' in GMT' => 'fetch_api_reseller_bookings_new_in_gmt_by_agency',
      ' with fractional cancellation_cutoff_hours' => 'fetch_api_reseller_bookings_new_fractional_cancellation_cutoff_hours'
    }
    options[:cassette_name] = cassette_name_mapping[booking_option]
  end
  if booking_option == ' in GMT'
    options[:departure_time] = Time.parse("#{date_tomorrow} 09:00:00 +0000")
  end
  stub_endpoint_bookings_new(options)
  stub_endpoint_vacancy_requests_show
end

Then(/^I should have hidden input "(.*?)" filled with "(.*?)"$/) do |id, value|
  expect_to_have_hidden_input_filled_with(id, value)
end