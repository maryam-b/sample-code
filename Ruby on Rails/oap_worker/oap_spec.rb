# Unit test for model using Webmock to stub out external HTTP requests
require 'rails_helper'

RSpec.describe Oap, :type => :model do
  let(:user) {{first_name: Faker::Name.first_name, email: Faker::Internet.email}}
  before {
    stub_request(:post, "URL").
      with( :body => "uid=#{Figaro.env.oap_uid}&firstname=#{CGI.escape(user[:first_name])}&email=#{CGI.escape(user[:email])}",
            :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
      to_return(:status => 200, :body => "", :headers => {})
  }

  it "send user's data to Oathkeeper" do
    oap_account = Oap.new
    response = oap_account.oap_opt_in(user[:first_name], user[:email])
    expect(response.code).to eq(200)
  end
  
end
