# Example of using sidekiq to run background job
class OapOptInWorker
  include Sidekiq::Worker

  def perform(first_name, email)
    Oap.new.oap_opt_in(first_name, email)
  end
end
