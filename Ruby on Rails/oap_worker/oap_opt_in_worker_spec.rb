# Unit test for Worker
describe OapOptInWorker do
  let!(:user) { create(:user, { first_name: Faker::Name.first_name, email: Faker::Internet.email }) }

  before do
    allow_any_instance_of(Oap).to receive(:oap_opt_in).and_return(true)
  end

  it 'calls oap_opt_in on an Oap instance' do
    expect_any_instance_of(Oap).to receive(:oap_opt_in).once

    OapOptInWorker.new.perform(user.first_name, user.email)
  end
end
