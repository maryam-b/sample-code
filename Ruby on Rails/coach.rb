# Create coach admin panel using ActiveAdmin
ActiveAdmin.register Coach do
  permit_params :first_name, :last_name, :avatar, :bio, :description, category_ids: [],
  videos_attributes: [:title, :subtitle, :description, :teaser, :asset, :pdf, :overmind_url, :affiliate_link, tag_ids: [], category_ids: [] ]

  controller do

    def create
      videos_attributes = params['coach']['videos_attributes']
      @coach = Coach.create!(coach_params)
      unless videos_attributes.nil?
        videos_attributes.each do |k, v|
          @video = @coach.videos.create!(v)
          @overmind_asset   = Asset.create(name: 'greatly123', source_url: Figaro.env.aws_course+@video.asset)
          @overmind_teaser  = Asset.create(name: 'greatly123', source_url: Figaro.env.aws_teaser+@video.teaser)
          @overmind_bio     = Asset.create(name: 'greatly123', source_url: Figaro.env.aws_bio+@video.bio)
          @overmind_pdf     = Asset.create(name: 'greatly123', source_url: Figaro.env.aws_pdf+@video.pdf)
          @video.update_attributes(overmind_asset: @overmind_asset.attributes.to_json, overmind_teaser: @overmind_teaser.attributes.to_json, overmind_pdf: @overmind_pdf.attributes.to_json, overmind_bio: @overmind_bio.attributes.to_json)
        end
      end
      redirect_to admin_coach_path(@coach)
    end

    def update
      videos_attributes = params['coach']['videos_attributes']
      @coach = Coach.find(params['id'])
      if @coach.update_attributes(coach_params)
        unless videos_attributes.nil?
          videos_attributes.each do |k, v|
            v.delete('_destroy')
            if v['id']
              @video = Video.find(v['id'])
              @video.update_attributes(v)
            else
              @video = @coach.videos.create(v)
            end
            @overmind_asset   = Asset.create(name: ‘NAME’, source_url: Figaro.env.aws_course+@video.asset)
            @overmind_teaser  = Asset.create(name: 'NAME', source_url: Figaro.env.aws_teaser+@video.teaser)
            @overmind_bio     = Asset.create(name: 'NAME', source_url: Figaro.env.aws_bio+@video.bio)
            @overmind_pdf     = Asset.create(name: 'NAME', source_url: Figaro.env.aws_pdf+@video.pdf)
            @video.update_attributes(overmind_asset: @overmind_asset.attributes.to_json, overmind_teaser: @overmind_teaser.attributes.to_json, overmind_pdf: @overmind_pdf.attributes.to_json)
          end
        end
      end
      redirect_to admin_coach_path(@coach)
    end

    private

    def coach_params
      params[:coach].permit(:first_name, :last_name, :bio, :avatar, :description, category_ids: [])
    end
  end

  form html: {multipart: true} do |f|
    @s3 = Upfinder.new
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :bio
      f.input :description
      f.input :avatar
      f.input :categories, collection: Category.where("parent_id is not null")
      f.has_many :videos, :allow_destroy => true do |video|
        video.input :title
        video.input :subtitle
        video.input :description
        video.input :asset,  as: :select,  collection: @s3.course
        video.input :teaser, as: :select,  collection: @s3.teaser
        video.input :pdf,    as: :select,  collection: @s3.pdf
        video.input :bio,    as: :select,  collection: @s3.bio
        video.input :categories, collection: Category.where("parent_id is not null")
        video.input :tags, as: :select
        video.input :affiliate_link
      end
    end
    f.actions
  end

  index do
    selectable_column
    column :first_name
    column :last_name
    column :bio
    column :description
    actions
  end

  show do |coach|
    attributes_table do
      row :avatar do
        image_tag(coach.avatar.url, width: 200, height: 250)
      end
      row :first_name
      row :last_name
      row :bio
      row :description
      row 'coach_specialties' do |n|
        coach.categories.map(&:name).join("<br />").html_safe
      end
    end
    render "video"
  end

end
