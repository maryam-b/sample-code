require 'spec_helper'

describe Booking::ProviderRemoteBooking::BlbBooking do
  describe '#save!' do
    include_context :models_booking_provider_remote_booking_blb_booking

    context 'with valid params' do
      around(:each) do |example|
        VCR.use_cassette('blb_multiple_booking_request') do
          VCR.use_cassette('blb_booking_vacancy_request') do
            example.run
          end
        end
      end

      it 'saves provider_booking_number' do
        expect(booking.provider_booking_number).to eq('5001091')
      end

      describe '#passengers' do
        let(:passenger) { booking.passengers.first }
        it 'saves passenger#provider_ticket_number' do
          expect(passenger.provider_ticket_number).to eq('60000451')
        end

        it '#provider_qr_code_string' do
          expect(passenger.provider_qr_code_string).to eq('600004511')
        end
      end
    end
  end
end
