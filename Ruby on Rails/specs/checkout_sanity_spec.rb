# Sanity spec using Capybara

require 'spec_helper'

describe 'checkout sanity check', :type => :feature , :js => true do

    context 'singlestep checkout', type: :feature do

      context 'has Physical product', vcr: { cassette_name: 'checkout_sanity_check_feature_1' } do
          before :each do
            # bundle 123 has a Physical product
            visit '/checkout/singlestep?bundles=123'
          end

          it 'shows shipping address different checkbox' do
            page.should have_selector("input[type='checkbox']")
          end

          it 'shows default shipping price to US' do
            page.should have_selector("span[id='shipping_country_notification']")
            find(:xpath, '//*[@id="order_shipping"]').text.should eq('7.95')
          end

          it 'shows correct shipping price if shipping address different is checked' do
            check('different-shipping')
            shipping_country = 'Japan'
            select(shipping_country, from: 'order[shipping_country]')
            find(:xpath, '//*[@id="order_shipping"]').text.should eq('39.95')
          end

          it 'shows billing state based on selected country' do
            billing_country = 'CANADA'
            select(billing_country, from: 'order[billing_country]')
            expect(find(:xpath, '//*[@id="order_billing_state"]')).to have_content('Alberta')
          end

          it 'shows shipping state based on selected country' do
            check('different-shipping')
            shipping_country = 'CANADA'
            select(shipping_country, from: 'order[shipping_country]')
            expect(find(:xpath, '//*[@id="order_shipping_state"]')).to have_content('British Columbia')
          end

          it 'shows paypal express button' do 
            find(:xpath, '//*[@id="paypal-tab"]').click
            page.should have_selector("button[id='PaypalExpressSubmit']")
          end

          it 'shows Authorize.net badge' do
            page.should have_selector("div[class='authorize']")
            page.should have_selector("img[src='https://verify.authorize.net/anetseal/images/secure90x72.gif']")
          end

          it 'shows Mindvalley security badge' do
            page.should have_selector("a[href='http://secure.trust-guard.com/certificates/6972']")
            page.should have_selector("img[src='https://secure.trust-guard.com/seals/6972/scanned/small/']")
          end
          
          # ....... more example ........
      end

    end

    context 'normal checkout', type: :feature do


      context 'has only digital product step2 AuthorizeNetCim', vcr: {cassette_name: 'checkout_sanity_check_feature_4'} do
        before :each do
          # bundle 1234 has only Digital products
          visit '/?bundles=1234'
          fill_in('order[first_name]', :with => 'John')
          fill_in('order[email]', :with => 'John@example.com')
          billing_country = 'CANADA'
          select(billing_country, from: 'order[billing_country]')
          find(:xpath, '//*[@value="AuthorizeNetCim"]').click
        end

        it 'doesn\'t show shipping address different checkbox' do
            page.should_not have_selector("input[type='checkbox']")
        end

        it 'shows billing state based on selected country' do
            billing_country = 'UNITED KINGDOM'
            select(billing_country, from: 'order[billing_country]')
            expect(find(:xpath, '//*[@id="order_checkout_state"]')).to have_content('Aberdeen City')
        end

        # ....... more example ........

      end

    end

end