require 'spec_helper'

describe Vacancy::BlbVacancy do
  include_context :models_vacancy_blb_vacancy
  before :each do
    Provider::PlaceCode.create(code: '2000', place: connection.departure_place, provider: connection.provider)
    Provider::PlaceCode.create(code: '6504', place: connection.arrival_place, provider: connection.provider)
  end

  context 'with valid params' do
    context 'successful response' do
      around(:each) do |example|
        VCR.use_cassette('blb_vacancy_soap_successful_request') do
          example.run
        end
      end

      context '#vacancy_known?' do
        it 'returns true' do
          expect(vacancy.vacancy_known?).to be true
        end
      end

      context '#vacant?' do
        it 'returns true' do
          expect(vacancy.vacant?).to be true
        end
      end
    end
    context 'get prices successful response' do
      around(:each) do |example|
        VCR.use_cassette('blb_vacancy_ticket_prices') do
          example.run
        end
      end
      context '#ticket_prices' do
        it 'returns same value as response' do
          expect(vacancy.ticket_prices.first.price_per_seat).to eq(Money.new(2500, provider.currency))
        end
      end

      context '#total_price' do
        let(:total_price) { Money.new(7100, 'EUR') }
        it 'returns same total_price' do
          expect(vacancy.total_price).to eq total_price
        end
      end

      context 'one ticket type' do
        it 'returns same value as response' do
           VCR.use_cassette('blb_vacancy_one_ticket_prices') do
              expect(vacancy.ticket_prices.first.price_per_seat).to eq(Money.new(2500, provider.currency))
          end
        end
      end
    end
  end

  context 'not successful' do
    context 'API returns error code' do
      it 'raises unexpected response error' do
        VCR.use_cassette('blb_vacancy_return_error_code') do
          expect{ vacancy.vacancy_known? }.to raise_error(Booking::ProviderRemoteError::UnexpectedResponseError)
        end
      end
    end
    context 'API returns invalid soap envelope' do
      it 'raises unexpected response error' do
        VCR.use_cassette('blb_vacancy_soap_fault') do
          expect{ vacancy.vacancy_known? }.to raise_error(Booking::ProviderRemoteError::UnexpectedResponseError)
        end
      end
    end

    context 'with invalid params' do
      before do
        Provider::PlaceCode.where(code: '2000').take.update_attribute(:code, '57366')
      end
      context 'API returns error code' do
        it 'raises unexpected response error' do
          VCR.use_cassette('blb_vacancy_invalid_params') do
            expect{ vacancy.vacancy_known? }.to raise_error(Booking::ProviderRemoteError::UnexpectedResponseError)
          end
        end
      end
    end
  end
end
