# Unit test for Controller using Webmock to stub out external HTTP requests
require 'rails_helper'

RSpec.describe RegistrationsController, :type => :controller do

  context "GET #new" do
    context "User signed in to MVA" do
      let(:user) { FactoryGirl.create(:user) }
      before {
        controller.stub(:current_user).and_return(user)
      }

      it "redirect to dashboard path" do
        get :new
        expect(response).to redirect_to '/dashboard/index'
      end
    end

    context "User not signed in to MVA" do
      it "renders new template" do
        get :new
        expect(response).to render_template :new
      end
    end
  end

  describe "POST create" do
    context "User signed in to MVA" do
      let(:user) { FactoryGirl.create(:user, password: 'password') }
      before {
        controller.stub(:current_user).and_return(user)
      }

      it "redirect to root path" do
        post :create, { user: { first_name: user.first_name, last_name: user.last_name, email: user.email, password: 'password', lang: user.lang } }

        expect(response).to redirect_to "/dashboard/index"
      end
    end

    context "User not signed in to MVA" do
      context "success" do
        let(:user) {{ first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email, password: "password", password_confirmation: "password", lang: "en"}}
        before {
          stub_request(:post, Figaro.env.mva_url + "/api/1/users").
            with( :query => {api_id: Figaro.env.mva_app_id, api_key: Figaro.env.mva_app_key},
                  :body => "first_name=#{CGI.escape(user[:first_name])}&last_name=#{CGI.escape(user[:last_name])}&email=#{CGI.escape(user[:email])}&password=#{CGI.escape(user[:password])}&password_confirmation=#{CGI.escape(user[:password_confirmation])}&lang=#{CGI.escape(user[:lang])}",
                  :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
            to_return(:status => 200, :body => "", :headers => {})
          }

        it "returns user json" do
          post :create, :user => user
          expect(response).to redirect_to '/users/auth/mindvalley'
          expect(OapOptInWorker).to have_enqueued_job(user[:first_name], user[:email])
        end
      end

      context "invalid" do
        let(:user) {{ first_name: "bad", email: "crap", password: "pass", lang: "en"}}
        before {
          stub_request(:post, Figaro.env.mva_url + "/api/1/users").
            with( :query => {api_id: Figaro.env.mva_app_id, api_key: Figaro.env.mva_app_key},
                  :body => "first_name=#{CGI.escape(user[:first_name])}&email=#{CGI.escape(user[:email])}&password=#{CGI.escape(user[:password])}&lang=#{CGI.escape(user[:lang])}",
                  :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
            to_return(:status => 422, :body => "a whole bunch of html codes", :headers => {})
        }

        it "render back new template" do
          post :create, :user => user

          expect(response).to render_template :new
        end
      end
    end
  end

end
