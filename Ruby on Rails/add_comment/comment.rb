class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment
  validates :comment, presence: true

  belongs_to :commentable, :polymorphic => true

  default_scope -> { order('created_at DESC') }

  # NOTE: Comments belong to a user
  belongs_to :user

  def self.find_comments_by_user(user)
    where(["user_id = ?", user.id]).order("created_at DESC")
  end

end
