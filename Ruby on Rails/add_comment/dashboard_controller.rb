class DashboardController < ApplicationController
  before_filter :authenticate_user!
  def index
    @video_of_the_day = Schedule.video_of_the_day(DateTime.now.in_time_zone('UTC'))
    unless @video_of_the_day.blank?
      @commentable = @video_of_the_day
      unless @commentable.comments.find_by_user_id(current_user).blank?
        @comment = @commentable.comments.find_by_user_id(current_user)
      else
        @comment = @commentable.comments.build(user_id: current_user)
      end
      @comments = @commentable.comments.limit(10)
    end
  end
end
