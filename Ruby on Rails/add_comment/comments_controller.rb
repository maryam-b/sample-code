class CommentsController < ApplicationController
  before_filter :authenticate_user!
  def create
  	@comment = Comment.new(comment_params)
  	@comment.user_id = current_user.id
    if @comment.save
  		redirect_to '/dashboard/index'
  	else
  		redirect_to '/dashboard/index', notice: 'something went wrong'
  	end
  end

  def update
  	@comment = Comment.find(params[:id])
  	if @comment.update_attributes(comment_params)
  		redirect_to '/dashboard/index'
    else
      redirect_to '/dashboard/index', notice: 'something went wrong'
  	end
  end

  private

  def comment_params
    params[:comment].permit(:comment, :commentable_id, :commentable_type)
  end

end