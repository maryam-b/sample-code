require 'rails_helper'

RSpec.describe CommentsController, :type => :controller do

	let(:comment) {FactoryGirl.create(:comment)}
	let(:user) { FactoryGirl.create(:user) }
	let(:current_user) {user}
	describe 'POST #create' do

		context 'when user is logged in' do
			before { allow(controller).to receive(:current_user).and_return(user) }

			context 'valid attributes' do
				it 'saves the comment in database' do
					expect{
						post :create, comment: attributes_for(:comment)
					}.to change(Comment, :count).by(1)
				end

				it 'redirects to dashboard#index' do
					post :create, comment: attributes_for(:comment)
					expect(response).to redirect_to '/dashboard/index'
				end
			end

			context 'invalid attributes' do
				it 'does not save the new comment in the database' do
					expect{
						post :create,
						comment: FactoryGirl.attributes_for(:comment, comment: '')
					}.to_not change(Comment, :count)
				end
			end
		end

		context 'when user is not logged in' do

			it 'redirects to mindvalley login page' do
				post :create, comment: FactoryGirl.attributes_for(:comment)
				expect(response).to redirect_to root_path
			end
		end
	end

	describe 'PATCH #update' do
		context 'when the user is logged in' do
			before { allow(controller).to receive(:current_user).and_return(user) }

			context 'valid attributes' do
				it 'updates the comments attributes and redirects to user dashboard' do
					patch :update, id: comment,
						comment: attributes_for(:comment,
						comment: 'Update this comment')
					comment.reload
					expect(comment.comment).to eq('Update this comment')
					expect(response).to redirect_to('/dashboard/index')
				end
			end

			context 'invalid attributes' do
				it 'does not change comments attributes' do
					patch :update, id: comment,
						comment: attributes_for(:comment,
						comment: '')
					comment.reload
					expect(comment.comment).to_not eq('')
				end
			end
		end

		context 'when the user is not logged in' do
			it 'redirects to mindvalley login page' do
				patch :update, id: comment, comment: attributes_for(:comment)
				expect(response).to redirect_to root_path
			end
		end
	end
end
