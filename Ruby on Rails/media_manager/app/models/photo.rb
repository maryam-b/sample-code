class Photo < ActiveRecord::Base
  has_attached_file :photo, :styles => { :medium => "300x300>", :thumb => "50x50>" },
                    :url  => "/assets/photos/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/assets/photos/:id/:style/:basename.:extension"

  validates_attachment_presence :photo

  validates_attachment_size :photo, :less_than => 5.megabytes
  validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/png']
  validates :title, presence: true

  belongs_to :user
  scope :by_user, ->(user_id) { where(user_id: user_id) }
  scope :search_by, ->(search) { where( "title LIKE ? ", "%#{search}%")}

end
