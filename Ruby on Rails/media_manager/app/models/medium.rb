class Medium < ActiveRecord::Base
  validates :title, presence: true
  validates :content, presence: true

  belongs_to :user

  scope :by_user, ->(user_id) { where(user_id: user_id) }
  scope :search_by, ->(search) { where( "title LIKE ? ", "%#{search}%")}

end
