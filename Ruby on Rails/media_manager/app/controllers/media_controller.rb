class MediaController < ApplicationController
  before_action :logged_in_user
  before_filter :find_params, only: [:show, :edit, :update, :destroy]

  def index
    if params[:search]
      @media = Medium.by_user(current_user.id).search_by(params[:search])
      @photos = Photo.by_user(current_user.id).search_by(params[:search])
    else
      @media = Medium.by_user(current_user.id)
      @photos = Photo.by_user(current_user.id)
    end
  end

  def new
    @medium = Medium.new
  end

  def create
    @medium = Medium.new(media_params)
    @medium.user_id = current_user.id
    if @medium.save
      redirect_to @medium
    else
      redirect_to media_path, alert: 'something went wrong'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @medium.update(media_params)
      redirect_to @medium
    else
      render 'edit'
    end
  end

  def destroy
    @medium.destroy

    redirect_to media_path
  end

  private

  def media_params
    params[:medium].permit(:title, :content)
  end

  def logged_in_user
    unless logged_in?
      redirect_to login_url
    end
  end

  def find_params
    @medium = Medium.find(params[:id])
  end
end