class PhotosController < ApplicationController
  before_action :logged_in_user

  def index
    @photos = Photo.by_user(current_user.id)
  end

  def new
    @photo = Photo.new
  end

  def create
    @photo = Photo.new(photo_params)
    @photo.user_id = current_user.id
    if @photo.save
      redirect_to @photo
    else
      flash.now[:alert] = "something went wrong!"
      render 'new'
    end
  end

  def show
    @photo = Photo.find(params[:id])
  end

  private

  def photo_params
    params[:photo].permit(:title,:photo)
  end

  def logged_in_user
    unless logged_in?
      redirect_to login_url
    end
  end
end