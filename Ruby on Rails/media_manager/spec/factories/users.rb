FactoryGirl.define do
  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password {'mind1test'}
    password_confirmation {'mind1test'}
  end
end