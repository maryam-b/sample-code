require 'spec_helper'

describe UsersController do

  let(:user) { FactoryGirl.create(:user) }
  describe 'POST #create' do

    context 'valid attributes' do
      it 'saves user in database' do
        expect{
          post :create, user: attributes_for(:user)
        }.to change(User, :count).by(1)
      end

      # it 'redirects to user profile page' do
      #   post :create, user: attributes_for(:user)
      #   expect(response).to redirect_to  "/users/#{user.id}"
      # end
    end

    context 'invalid attributes' do
      it 'does not save the new user in the database' do
        expect{
          post :create,
          user: FactoryGirl.attributes_for(:user, password: '')
        }.to_not change(User, :count)
      end
    end

    it 'renders the #show view' do
        get :show, id: user
        response.should render_template :show
    end
  end
end
