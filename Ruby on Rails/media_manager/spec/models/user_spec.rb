require 'spec_helper'

RSpec.describe User, :type => :model do

  it 'is valid with a name, email, password, password_confirmation' do
    user = FactoryGirl.build(:user)
    expect(user).to be_valid
  end

  it 'is invalid without a name' do
    user = FactoryGirl.build(:user, name: nil)
    expect(user).to be_invalid
  end

  it 'is invalid without a email' do
    user = FactoryGirl.build(:user, email: nil)
    expect(user).to be_invalid
  end

  it 'is invalid without a password' do
    user = FactoryGirl.build(:user, password: nil)
    expect(user).to be_invalid
  end

end
