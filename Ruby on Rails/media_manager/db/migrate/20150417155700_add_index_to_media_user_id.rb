class AddIndexToMediaUserId < ActiveRecord::Migration
  def change
    add_index :media, :user_id, using: :btree
  end
end
