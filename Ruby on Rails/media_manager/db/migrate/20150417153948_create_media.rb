class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.integer :user_id
      t.string :name
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
