class RemoveNameFromMedia < ActiveRecord::Migration
  def change
    remove_column :media, :name, :string
  end
end
