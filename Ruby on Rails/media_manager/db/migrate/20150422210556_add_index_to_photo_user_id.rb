class AddIndexToPhotoUserId < ActiveRecord::Migration
  def change
    add_index :photos, :user_id, using: :btree
  end
end
