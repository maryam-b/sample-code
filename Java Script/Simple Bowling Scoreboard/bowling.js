function calculate_score(first_score,second_score,last_score_status){
  if (last_score_status == 'strike'){
    return (first_score+second_score)*2;

  }else if(last_score_status=='spare'){
    return (first_score*2)+second_score;
  }else{
    return first_score+second_score;
  }
}


function rand_roll(number_count){
  value=Math.floor((Math.random() * (number_count+1)));
  return value ;
}

function show(frame,roll,score){
  if(roll==1){
    if(score==10)
      score="X";
    document.getElementById(""+frame+roll+"").innerHTML=score;
  }
  if(roll==2){
    if(score==0 )
      score='-';
      document.getElementById(""+frame+roll+"").innerHTML=score;

  }

  if(roll==3){
      document.getElementById(""+frame+roll+"").innerHTML=score;
  }
}

function main(){
  total=0;
  document.write("<body style='background-color:blue;margin-top:100px'><center><table cellspacing='0' style='text-align:center' ><tr height='200px'  style='background-color:#CCC' >");
  for(i=1;i<=10;i++)
    document.write("<td id='"+i+"1' width='50px' style='border:solid 1px #FFF'></td><td id='"+i+"2' width='50px' style='border:solid 1px #FFF'></td><td rowspan='2' width='10px'></td>");
  document.write("</tr>");
  document.write("<tr height='100px' style='background-color:#CCC'>");
  for(i=1;i<=10;i++)
    document.write("<td id='"+i+"3' colspan='2' style='border:solid 1px #FFF'></td>");
  document.write("</tr></table></body>");
  document.write("<table ><tr style='background-color:#CCC'>");
  last_score_status = null;
  for (var i = 1; i <= 10; i++) {
    first_score = rand_roll(10);
    if(last_score_status=='spare')
      show(i-1,3,total+first_score);
    show(i,1,first_score);
    // This alert is just to show sequence of calculation.
    alert("frame"+i+" roll 1");
    if(first_score == 10){
      score_status = 'strike';
        second_score=null;
    }else{
      second_score = rand_roll(10-first_score);
      if (second_score+first_score == 10){
        score_status = 'spare';
        show(i,2,"/");
      }else{
        score_status = null;
        show(i,2,second_score);
      }
      // This alert is just to show sequence of calculation.
      alert("frame"+i+" roll 2");

    }
    total_score = first_score+second_score;
    if(last_score_status=='strike')
      show(i-1,3,total+total_score);
    last_total=total;
    total += calculate_score(first_score,second_score,last_score_status);
    if(total_score==10)
      show(i,3,last_total);
    else
      show(i,3,total);
    // This alert is just to show sequence of calculation.
    alert("Next Frame");
    last_score_status = score_status;
  }
  document.write("</tr></table></center>");
}

main();