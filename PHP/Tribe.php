<?php
class Tribe extends Lib{
    
    protected $mongoconn;
    protected $apitoken;
    protected $acctoken;

    /** Standard Lib Functions START **/
    function __construct()
    {    
        parent::__construct();

        $this->apitoken = 'XXXX';
        $this->urlbase = 'https://api.mongohq.com/';
        $this->database = 'Database_name ';
        $this->deployment_id = 'XXXX';
    }

    function scopeOf($functionName)
    {
        switch ($functionName){
            default:
                return '';
        }
    }
    /** Standard Lib Functions STOP **/

    /** Form Handlers START **/
    function requestSummary(){
      

     echo json_encode($this->_getSurveySummary());
      die();
    }

    function requestSubmit(){
  
      $data = $_POST;   
          //var_dump($data);
         // die();
      $Gvar_patt = '/^entry_[0-9]+/';
      foreach($data as $key => $val){
        if(preg_match($Gvar_patt, $key)){
          if(preg_match('/_option_/', $key)){
            $temp_key = str_replace('_','.',$key);
            $temp_key = str_replace('.option.','_option_',$temp_key);
            $data[$temp_key] = $val;
          }
          else{
            $data[str_replace('_','.',$key)] = $val;
            unset($data[$key]);
          }
        }
      }
      $submit_to = "http://url";
      Wrapper::httpRequest($submit_to,$data)->getBody();
      $test = self::processSurveyData($data);

      die();
    }
    /** Form Handlers STOP **/

    /** Tags Methods START **/
    function form($params){
     
      self::_getParam($params,'identifier',rand(1,9999));
      self::_getParam($params,'action',Wrapper::basePath().$this->_formAction('Submit'));
      self::_getParam($params,'next','/');
      self::_getParam($params,'tribe_id');

      return $this->_getHtmlTemplate(self::_getParam($params,'template','tribe.phtml'),$params);
    }
    /** Tags Methods STOP **/

    /** Helper Function START **/
    
    function _query($params=array()){

        $params['_apikey'] = $this->apitoken;

        $collection = $params['collection'];
        $user_action = $params['action'];
        $user_data = $params['data'];
        $user_criteria = $params['criteria'];

        unset($params['collection']);
        unset($params['action']);
        unset($params['data']);
        unset($params['criteria']);

        $endpoint = $this->urlbase . 'deployments/USERNAME/'. $this->deployment_id .'/mongodb/' . $this->database . '/collections/' . $collection . '/documents?' . http_build_query($params);
        $ch = curl_init($endpoint);
        // echo $endpoint . '<br />';
        
        if($user_action == 'insert'){
            $postdata = '';
            if(is_array($user_data)){
              $postdata = '{"document" : ' . json_encode($user_data) .', "w" : 0 }';
            }
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        }elseif($user_action == 'update' || $user_action == 'upsert'){
            $postdata = '';
            if(is_array($user_data)){
                if($user_action == 'upsert'){
                    $postdata = '{ "criteria": ' . json_encode($user_criteria) . ', "object" : ' . json_encode($user_data) .', "upsert": true , "update" : ' . json_encode($user_data) .'}';
                }else{
                    $postdata = '{ "criteria": ' . json_encode($user_criteria) . ', "object" : ' . json_encode($user_data) .' }';
                }
            }
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        }

        // echo $postdata . '<br />';
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Authorization: Bearer XXXX', 
                                                    'Content-type: application/json', 
                                                    'Accept-Version: 2014-06'));       
        $response = curl_exec($ch);
        curl_close($ch);
        
        if(!empty($response)){

            return json_decode($response);
        }else{
            return array('Error' => 1);
        }
    }

    function _insertVotes($params){
        $default = array(
            'collection' => 'CollectionName',
            'action' => 'insert',
        );
        $params = array_merge($default, $params);
        $res = $this->_query($params);
        
        return $res;
    }

    function _updateResults($params){
        $default = array(
            'collection' => 'CollectionName',
            'action' => 'update',
        );
        $params = array_merge($default, $params);
        $res = $this->_query($params);
        
        return $res;
    }

    /** Helper Function STOP **/

    /** Tags Methods START **/
    function _filterDataFields($params){
        
        $fields = array('uid' => $params['uid'],
                         'tribe_id' => $params['tribe_id'],
                         'tribe_name' => $params['tribe_name'],
                         'firstname' => $params['firstname'],
                         'email' => $params['email'],
                     );

        return $fields;
    }
    
    function _insertSurveyData($params){
        // Insert Survey Result
         
        $data_fields = $this->_filterDataFields($params);

        if(!empty($_COOKIE['oemail'])){
            $data_fields['email'] = $_COOKIE['oemail'];
        }

        $data = array(
            'data' => $data_fields
        );
        return $this->_insertVotes($data);
    }


    function _updateSummaryCount($params){

        $data_fields = array($params['tribe_id'] => 1);

        $data = array(
            'collection' => 'CollectionName',
            'action' => 'upsert',
            'criteria' => new stdClass(),
            'data' => array(
                '$inc' => $data_fields
            )
        );
        
        return $this->_updateResults($data);
    }

    function _getSurveySummary(){

        $params = array(
            'collection' => 'CollectionName',
            'limit' => 1,
            
        );
        
        $res = $this->_query($params);
        //var_dump($res);
        
        if(is_array($res))
            return array_pop($res);

        return false;
    }

    function processSurveyData($params){
        global $survey_summary;

        // Insert Survey Data
        $this->_insertSurveyData($params);

        // Update Survey Summary 
        $this->_updateSummaryCount($params);

        return '';
    }

    /** Tags Methods STOP **/
 
}