<?php

class PaymentManager {
  const MERCHANT_CODE = 'XXXXX';
  const MERCHANT_KEY = 'XXXXX';
  const Currency = 'MYR';
  const Lang = 'UTF-8';
  /***************************************PURCHASE METHODS *************************************************************/
 
  public static function createRefNo($userId,$nowDate,$chequenum=NULL){
    if($chequenum == NULL){
      $RefNo = $userId.$nowDate;
    }elseif ($chequenum != NULL) {
      $RefNo = $nowDate.'CN'.$chequenum;
    }
    
    return $RefNo;
  }
 
 public static function getMerchantCode() {
    return self::MERCHANT_CODE;
  }
  
 public static function getCurrency() {
    return self::Currency;
  }
  
  public static function getLang() {
    return self::Lang;
  }

 
  
  public static function insertPurchase($pdo, $usr_id,$dea_id,$brn_id,$payment_method,$token,$request_time,$user_agent,$IP,$ref_no,$amount,$quantity,$methods,$parent_id) {    
   // echo $usr_id." ".$brn_id." ".$payment_method." ".$token." ".$request_time." ".$user_agent." ".$IP." ".$ref_no." ".$amount." ".$quantity;
    /*if($brn_id == NULL){
      $brn_id = 10;
    }*/
     $sql = "INSERT INTO `purchase` (`usr_id`,`dea_id`,`brn_id`,`payment_method`,`token`,`request_time`,`user_agent`,`IP`,`ref_no`,`amount`,`quantity`,`methods`,`parent_id`) 
      VALUES (:usr_id, :dea_id, :brn_id, :payment_method, :token, FROM_UNIXTIME(:request_time), :user_agent, :IP, :ref_no, :amount, :quantity, :methods, :parent_id)";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(":usr_id", $usr_id);
    $statement->bindValue(":dea_id", $dea_id);
    $statement->bindValue(":brn_id", $brn_id);
    $statement->bindValue(":payment_method", $payment_method);
    $statement->bindValue(":token", $token);
    $statement->bindValue(":request_time", $request_time);
    $statement->bindValue(":user_agent", $user_agent);
    $statement->bindValue(":IP", $IP);
    $statement->bindValue(":ref_no", $ref_no);
    $statement->bindValue(":amount", $amount);
    $statement->bindValue(":quantity", $quantity);
    $statement->bindValue(":methods", $methods);
    $statement->bindValue(":parent_id", $parent_id);
    
    $affectedRows = $statement->execute();

    return array("id" => $pdo->lastInsertId(), "rows" => $affectedRows);
  }
  
  
    
  public static function readPurchase($pdo,$RefNo=NULL,$pur_id=NULL,$parent_id=NULL){
    //echo $parent_id;
    if($RefNo != NULL && $pur_id == NULL){
    $sql = "SELECT * FROM `purchase` WHERE `ref_no` = :RefNo";
    }else if ($pur_id != NULL && $RefNo == NULL){
      $sql = "SELECT * FROM `purchase` WHERE `pur_id` = :pur_id";
    }else if($parent_id != NULL && $pur_id == NULL && $RefNo == NULL){
      $sql = "SELECT * FROM `purchase` WHERE `parent_id` = :parent_id and pur_status = 'completed'" ;
    }
   // echo $sql;
    $statement = $pdo->prepare($sql);
    if($RefNo != NULL && $pur_id == NULL){
    $statement->bindValue(":RefNo", $RefNo);   
    }else if($pur_id != NULL && $RefNo == NULL){
    $statement->bindValue(":pur_id", $pur_id);
    }else if($parent_id != NULL && $pur_id == NULL && $RefNo == NULL){
    $statement->bindValue(":parent_id", $parent_id);
    }
    $statement->execute();
    $resultset = $statement->fetchAll();

    if ($resultset != null) {
      return $resultset[0];
    } else {
      return null;
    }
  }
  
  public static function readPurchaseBycheque($pdo,$chequeNum){
     
     $cheque_num = 'CN'.$chequeNum;
     $sql = "SELECT * FROM `purchase` WHERE `ref_no` like '%$cheque_num' and success = 2" ;
    

    $statement = $pdo->prepare($sql);
    $statement->execute();
    $resultset = $statement->fetchAll();
    if ($resultset != null) {
      return $resultset;
    } else {
      return null;
    }
  }
  
  public static function readPurchaseByDeal($pdo,$dea_id){
     
    $sql = "SELECT * FROM `purchase` WHERE `dea_id` = :dea_id  and success = 1 and pur_status = 'completed'" ;
    $statement = $pdo->prepare($sql);
    $statement->bindValue(":dea_id", $dea_id);
    $statement->execute();
    $resultset = $statement->fetchAll();
    if ($resultset != null) {
      return $resultset;
    } else {
      return null;
    }
  }
  
  public static function readallPurchase($pdo,$success,$pur_status=NULL){
    
    if($success == 1 && $pur_status == 'completed'){
      $sql = "select pur.pur_id,pur.parent_id,pur.ref_no,pur.payment_method,pur.dea_id,pur.respone_time,pur.usr_id,pur.quantity,pur.amount,pur.success,pur.token,d.title,u.username,u.email 
            from purchase pur
            inner join deal d on pur.dea_id = d.dea_id 
            inner join user u on u.usr_id = pur.usr_id
            where  pur.success in (1,2) and pur_status =:pur_status";
    }else if($success == 1 && $pur_status == 'pending'){
      $sql = "select pur.pur_id,pur.parent_id,pur.ref_no,pur.payment_method,pur.dea_id,pur.respone_time,pur.usr_id,pur.quantity,pur.amount,pur.success,pur.token,d.title,u.username,u.email 
            from purchase pur
            inner join deal d on pur.dea_id = d.dea_id 
            inner join user u on u.usr_id = pur.usr_id
            where  pur.success = :success and pur_status =:pur_status";
    }else if($success == 0){
      $sql = "select pur.pur_id,pur.parent_id,pur.ref_no,pur.payment_method,pur.dea_id,pur.respone_time,pur.usr_id,pur.quantity,pur.amount,pur.success,pur.token,d.title,u.username,u.email 
            from purchase pur
            inner join deal d on pur.dea_id = d.dea_id 
            inner join user u on u.usr_id = pur.usr_id
            where  pur.success = :success  ";
    }else if($success == 10){
      $sql = "select pur.pur_id,pur.parent_id,pur.ref_no,pur.payment_method,pur.dea_id,pur.respone_time,pur.usr_id,pur.quantity,pur.amount,pur.success,pur.token,d.title,u.username,u.email 
            from purchase pur
            inner join deal d on pur.dea_id = d.dea_id 
            inner join user u on u.usr_id = pur.usr_id
            where pur.success is NULL ";
    }else if($success == 2){
      $sql = "select pur.pur_id,pur.pur_status,pur.parent_id,pur.ref_no,pur.payment_method,pur.dea_id,pur.respone_time,pur.usr_id,pur.quantity,pur.amount,pur.success,pur.token,d.title,u.username,u.email 
            from purchase pur
            inner join deal d on pur.dea_id = d.dea_id 
            inner join user u on u.usr_id = pur.usr_id
            where  pur.success = :success ";
    }
    echo $sql;
    $statement = $pdo->prepare($sql);
    if($pur_status != NULL){
      $statement->bindValue(":pur_status", $pur_status);
    }
    if(!($success == 1 && $pur_status == 'completed')){
    $statement->bindValue(":success", $success);}
    $statement->execute();
    $resultset = $statement->fetchAll();

    if ($resultset != null) {
      return $resultset;
    } else {
      return null;
    }
  }
  
  public static function readPurchaseByuser($pdo,$usr_id,$success,$order = null, $page = null, $pageSize = null,$type = null){
    
    $sql = "select pur.pur_id,pur.dea_id,pur.request_time,pur.pur_status,pur.parent_id,pur.respone_time,d.title,d.pht_id1,d.promotion_price,d.original_price,d.end_time,d.current_quantity from purchase pur
            inner join deal d on pur.dea_id = d.dea_id 
            where usr_id = :usr_id and success = :success and parent_id in (1,0)";
    if ($order != null) {
      if($type != null){
       $sql .= " ORDER BY `$order` $type "; 
      }  else {
        $sql .= " ORDER BY `$order`";
      }     
    }
    if ($page != null && $pageSize != null) {
        $start = ($page - 1) * $pageSize;
        $end = $pageSize;

        $sql .= " LIMIT $start, $end";
      }
    //echo $sql;  
    $statement = $pdo->prepare($sql);
    $statement->bindValue(":usr_id", $usr_id);
    $statement->bindValue(":success", $success);
    $statement->execute();
    $resultset = $statement->fetchAll();

    if ($resultset != null) {
      return $resultset;
    } else {
      return null;
    }
  }
  
  public static function countPurchasePage($pageSize, $usr_id, $success) {
   
    $pdo = ConnectionManager::generatePDO();
    $alldeal = PaymentManager::readPurchaseByuser($pdo, $usr_id, $success);
    $counts_all = count($alldeal);
    $mod = ($counts_all % $pageSize);
    if ($mod == 0) {
      $pageNumber = $counts_all/$pageSize;
    } else if ($mod > 0) {
     $pageNumber = intval(($counts_all/$pageSize)) +1;
    }
    $list = $pageNumber%10;
    if ($list == 0) {
      $listNumber = $pageNumber/10;
    } else if ($list > 0) {
     $listNumber = intval(($pageNumber/10)) +1;
    }

    $result = array ("pageNumber" => $pageNumber,"listNumber" => $listNumber,"counts_all" => $counts_all);
    return $result;
  }
  
  public static function updatePurchase($pdo, $pur_id,$ref_no,$status,$respone_time,$transaction_id,$pur_status,$err_desc=NULL,$amount=NULL) {
   //echo $pur_id." ".$ref_no." ".$status." ".$respone_time." ".$transaction_id." ".$pur_status;
    $condition =" `pur_id` = :pur_id and `ref_no` = :ref_no ";
    
    $sql = "UPDATE `purchase` SET `success` = :status, `respone_time` = FROM_UNIXTIME(:respone_time), `transaction_id` = :transaction_id, `pur_status` = :pur_status, `err_desc` = :err_desc   ";
    if($amount!= NULL){
      $sql .= ", `amount` = :amount where $condition";
    }else{
      $sql .= " where $condition";
    }
    //echo $sql;
    $statement = $pdo->prepare($sql);
    $statement->bindValue(":pur_id", $pur_id);
    $statement->bindValue(":ref_no", $ref_no);
    if($amount!= NULL){
      $statement->bindValue(":amount", $amount);
    }
    $statement->bindValue(":status", $status);
    $statement->bindValue(":respone_time", $respone_time);
    $statement->bindValue(":transaction_id", $transaction_id);
    $statement->bindValue(":pur_status", $pur_status);
    $statement->bindValue(":err_desc", $err_desc);

    $affectedRows = $statement->execute();

    return $affectedRows;
  }
  

  
  public static function createSignature($RefNo,$Amount,$Currency){
     
    $rawSignature = self::MERCHANT_KEY . self::MERCHANT_CODE . $RefNo . str_replace('.', '', $Amount) . $Currency;
    $signature = self::iPay88_signature($rawSignature);
    return $signature;
  }

 public static function createResponseSignature($payment_id,$RefNo,$Amount,$status){
     
    $rawSignature = self::MERCHANT_KEY . self::MERCHANT_CODE .$payment_id. $RefNo . str_replace('.', '', $Amount) . self::Currency.$status;
     $signature = self::iPay88_signature($rawSignature);
    return $signature;
  }
        
  private static function iPay88_signature($source) {
    return base64_encode(self::hex2bin(sha1($source)));
  }
  
  private static function hex2bin($hexSource) {
    $bin = "";
    for ($i = 0; $i < strlen($hexSource); $i = $i + 2) {
      $bin .= chr(hexdec(substr($hexSource, $i, 2)));
    }
    return $bin;
  }
  
  public static function get_payment_method($payment_id){
    
    if ($payment_id == 2){
      $payment_method = "creditcard";
    }elseif ($payment_id == 6){
      $payment_method = "maybank";
    }elseif($payment_id == 20){
       $payment_method = "cimb";
    }elseif($payment_id == 14){
       $payment_method = "RHB";
    }elseif($payment_id == 23){     
       $payment_method = "meps";
    }elseif($payment_id == 10){
       $payment_method = "ambank";
    }elseif($payment_id == 15){
       $payment_method = "hong";
    }elseif($payment_id == 0){
      $payment_method = "cheque";
    }
    return $payment_method;
  }
  
   public static function get_payment_id($payment_method){
    
    if ($payment_method == "creditcard"){
      $payment_id = 2;
    }elseif ($payment_method == "maybank"){
      $payment_id = 6;
    }elseif($payment_method == "cimb"){
       $payment_id = 20;
    }elseif($payment_method == "RHB"){
       $payment_id = 14;
    }elseif( $payment_method == "meps"){     
      $payment_id = 23;
    }elseif($payment_method == "ambank"){
       $payment_id = 10;
    }elseif($payment_method == "hong"){
       $payment_id = 15;
    }elseif($payment_method == "cheque"){
      $payment_id = 0;
    }
    return $payment_id;
  }
}

?>
