<!-- Add deal - Admin panel -->
<div id="mainContent">
  {if $loggedIn}
    {if $loggedInUser.role ge 5}
      <div id="pageTitle">{#newDeals#}</div>
      <div class="form">
        <script language="javascript" type="text/javascript">
          tinyMCE.triggerSave();
          tinyMCE.init({
            // General options
            mode : "textareas",
            theme : "advanced",
            elements :"highlight,terms,description,shortDescription",
            plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            // Theme options
            theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : false,

            // Skin options
            skin : "o2k7",
            skin_variant : "silver"
            
        });      
       </script>
        <form id="frmNewDeals" enctype="multipart/form-data" method="post">
          <table class="form">
            <tr>
              <td class="caption">{#title#}:</td>
              <td class="data">
                <input type="text" name="title" id="title" maxlength="500" style="width: 500px" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#originalPrice#}:</td>
              <td class="data">
                <input type="number " name="originalPr" id="originalPr" style="width: 50px"  /> 
                <span>{#examplePrice#}</span>
              </td>
            </tr>
            <tr> 
              <td class="caption">{#promotionPrice#}:</td>
              <td class="data">
                <input type="number " name="promotionPr" id="promotionPr" style="width: 50px"  />
                 <span>{#examplePrice#}</span>
              </td>
            </tr>
            <tr> 
              <td class="caption">{#minQuantity#}:</td>
              <td class="data">
                <input type="number " name="minimumQ" id="minimumQ" style="width: 30px" /> 
              </td>
            </tr>
            <tr> 
              <td class="caption">{#maxQuantity#}:</td>
              <td class="data">
                <input type="number " name="maximumQ" id="maximumQ" style="width: 30px" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#shortDes#}:</td>
              <td class="data">
                <textarea class="data" name="shortDescription" id="shortDescription"></textarea>
              </td>
            </tr>
            <tr>
              <td class="caption">{#highlight#}:</td>
              <td class="data">
                <textarea class="data" name="highlight" id="highlight"></textarea>
              </td>
            </tr>
            <tr>
              <td class="caption">{#terms#}:</td>
              <td class="data">
                <textarea class="data" name="terms" id="terms"></textarea>
              </td>
            </tr>
            <tr>
              <td class="caption">{#description#}:</td>
              <td class="data">
                <textarea class="data" name="description" id="description"></textarea>
              </td>
            </tr>
            <tr>
              <td class="caption">{#photo#}1:</td>
              <td class="data">               
                <input type="file" name="file_upload1" id="file_upload1"  /> Supported formats: JPEG, GIF, PNG
                <input type="hidden" name="pht_id1" id="pht_id1" value=0 />  
                <div id="image1_value" name="image1_value" ></div>
                <div id="board_image1"></div>
              </td>
            <tr>
              <td class="caption">{#photo#}2:</td>
              <td class="data">
                <input type="hidden" name="MAX_FILE_SIZE" value="0" />
                <input type="file" name="file_upload2" id="file_upload2" /> Supported formats: JPEG, GIF, PNG
                <input type="hidden" name="pht_id2" id="pht_id2" value=0 />  
                <div id="image2_value" name="image2_value"></div>
                <div id="board_image2"></div>
              </td>
            </tr>
            <tr>
              <td class="caption">{#states#}:</td>
              <td class="data">
                <select name="stateId" id="stateId">
                    <option value="0">{#select#}</option>
                    {foreach from=$states item=state}
                    <option value="{$state.sta_id}">{$state.name}</option>
                    {/foreach}                
                  </select>
              </td>
            </tr>
            <tr>
              <td class="caption">{#category#}:</td>
              <td class="data">
                <select name="categoryId" id="categoryId">
                    <option value="0">{#select#}</option>
                    {foreach from=$categories item=category}
                    <option value="{$category.dct_id}">{$category.title}</option>
                    {/foreach}                
                  </select>
              </td>
            </tr>
            <tr>
              <td class="caption">{#merchant#}:</td>
              <td class="data">
                <select name="merchantId" id="merchantId">
                    <option value="0">{#select#}</option>
                    {foreach from=$merchants item=merchant}
                    <option value="{$merchant.mct_id}">{$merchant.company_name}</option>
                    {/foreach}                
                  </select>
              </td>
            </tr>
            <tr>
              <td class="caption">{#branches#}:</td>
              <td class="data">
                <select name="branchId" id="branchId">
                  <option value="0">{#select#}</option>
                </select>
              </td>
            </tr>
            <tr>
              <td class="caption">{#startDate#}:</td>
              <td class="data">
                <input type="text" name="startDate" id="startDate" /> 
                <span>{#exampleDate#}</span>
              </td>
              </tr>
              <tr>
              <td class="caption">{#endDate#}:</td>
              <td class="data">
                <input type="text" name="endDate" id="endDate" />
                <span>{#exampleDate#}</span>
              </td>
            </tr>
            <tr>
              <td class="caption">{#westCost#}:</td>
              <td class="data">
                <input type="number " name="westCost" id="westCost" style="width: 50px" /> 
                <span>{#examplePrice#}</span>
              </td>
            </tr>
            <tr>
              <td class="caption">{#eastCost#}:</td>
              <td class="data">
                <input type="number " name="eastCost" id="eastCost" style="width: 50px"  /> 
                <span>{#examplePrice#}</span>
              </td>
            </tr>
            <tr>
              <td class="caption">{#redeemStartDate#}:</td>
              <td class="data">
                <input type="text" name="redeemStartDate" id="redeemStartDate" /> 
                <span>{#exampleDate#}</span>
              </td>
              </tr>
              <tr>
              <td class="caption">{#redeemEndDate#}:</td>
              <td class="data">
                <input type="text" name="redeemEndDate" id="redeemEndDate" />
                <input type="hidden" name="rdm_id" id="rdm_id" value="0" />
                <span>{#exampleDate#}</span>
              </td>
            </tr>
            <tr>
              <td class="buttons" colspan="2">
                <input type="hidden" name="inputId"  id="inputId" value=0 />
                <input type="hidden" name="confirmed"  id="confirmed" value=0 />
                <input type="hidden" value="" name="highlight_mce" id="highlight_mce">
                <input type="hidden" value="" name="description_mce" id="description_mce">
                 <input type="hidden" value="" name="shortDesc_mce" id="shortDesc_mce">
                <input type="hidden" value="" name="terms_mce" id="terms_mce">
                <input type="submit" class="button" value="{#save#}" name="ok" id="ok"/>
                <input type="reset" class="button" value="{#new#}" id="new" />
              </td>          
            </tr>
            <tr>
              <td colspan="2">
                <div id="board"></div>
              </td>          
            </tr>
          </table>
        </form>
              
        <script type="text/javascript">
        
        var deleteDialogCaption = "{#delete#}";
        var tblDeals = null;
        focus('title');
        
        $('#new').click(function() {
            $("#inputId").val(0);
            $("#pht_id1").val(0);
            $("#pht_id2").val(0);
            $("#confirmed").val(0);
            $("#board").html("");
            $("#image1_value").html("");
            $("#image2_value").html("");
            focus('title');
        });
        $(function() {
            $('#startDate').datetimepicker({
                  controlType: 'select'
                  
            });
            $('#endDate').datetimepicker({
                  controlType: 'select'
                  
            });
            $('#redeemStartDate').datetimepicker({
                  controlType: 'select'
                  
            });
            $('#redeemEndDate').datetimepicker({
                  controlType: 'select'
                  
            });
	    $('#file_upload1').uploadify({
		'uploader'  : '/js/uploadify-php/js/uploadify.swf',
                'script'    : '/page/admin/upload.php',
                'cancelImg' : '/js/uploadify-php/js/cancel.png',
                'auto'      : true,
                'onComplete' : function(event, ID, fileObj, response, data) {
                 var input = $('<input>', {
                        id: 'deletePic1',
                        name: 'deletePic1',
                        type: 'button',
                        value: 'Delete',
                        click: function() {
                          var board = $("#board_image1");
                          var params = "id=" + response; 
                          $("#image1_value").html("");
                          var url = "/act/admin/deletephoto?ts" + new Date().getTime();
                          $.ajax({
                            type: "post",
                            url: url,
                            data: params,
                            success: function(data, textStatus, jqXHR) {            
                              board.html(data.message);
                              board.removeClass("warning error info");
                              if (data.result == 0) {
                                board.addClass("warning");
                              } else if (data.result == -1) {
                                board.addClass("error");
                              } else if (data.result == 1) {
                                board.addClass("info");              
                              }          

                              if (data.redirect != null && data.redirect != undefined) {
                                document.location = data.redirect;
                              }
                            },        

                            error: function(objAJAXRequest, strError) {
                              if (strError == "timeout") {
                                console.info("Timeout");
                                board.addClass("error");
                                board.html("Connection timed out!");
                              } else {
                                board.addClass("error");
                                board.html("Unknown error!");
                              }
                            }

                          }).done(function(msg) {
                            console.info( "Response: " + msg);        
                          });
                        }
                   })
                  $('#pht_id1').val(response);
                  $("#image1_value").html("<img width='50' height='50' src='/photo?id="+response+"&width=50&height=50&file=$filename'>");
                  $("#image1_value").append(input);
                  }
	    });
            $('#file_upload2').uploadify({
		'uploader'  : '/js/uploadify-php/js/uploadify.swf',
                'script'    : '/page/admin/upload.php',
                'cancelImg' : '/js/uploadify-php/js/cancel.png',
                'auto'      : true,
                'onComplete' : function(event, ID, fileObj, response, data) {
                  var input = $('<input>', {
                        id: 'deletePic2',
                        name: 'deletePic2',
                        type: 'button',
                        value: 'Delete',
                        click: function() {
                          var board = $("#board_image2");
                          var params = "id=" + response; 
                          $("#image2_value").html("");
                          var url = "/act/admin/deletephoto?ts" + new Date().getTime();
                          $.ajax({
                            type: "post",
                            url: url,
                            data: params,
                            success: function(data, textStatus, jqXHR) {            
                              board.html(data.message);
                              board.removeClass("warning error info");
                              if (data.result == 0) {
                                board.addClass("warning");
                              } else if (data.result == -1) {
                                board.addClass("error");
                              } else if (data.result == 1) {
                                board.addClass("info");              
                              }          

                              if (data.redirect != null && data.redirect != undefined) {
                                document.location = data.redirect;
                              }
                            },        

                            error: function(objAJAXRequest, strError) {
                              if (strError == "timeout") {
                                console.info("Timeout");
                                board.addClass("error");
                                board.html("Connection timed out!");
                              } else {
                                board.addClass("error");
                                board.html("Unknown error!");
                              }
                            }

                          }).done(function(msg) {
                            console.info( "Response: " + msg);        
                          });
                        }
                   })
                  $('#pht_id2').val(response);
                  $("#image2_value").html("<img width='50' height='50' src='/photo?id="+response+"&width=50&height=50&file=$filename'>");
                  $("#image2_value").append(input);
                  }
	    });
		
	});
        
        $('#frmNewDeals').submit(function() {  
           
          $('#highlight_mce').val(tinyMCE.get('highlight').getContent());
          $('#description_mce').val(tinyMCE.get('description').getContent());
           $('#shortDesc_mce').val(tinyMCE.get('shortDescription').getContent());
          $('#terms_mce').val(tinyMCE.get('terms').getContent());
          var board = $("#board");      
          var params = $("#frmNewDeals").serialize(); 

          var url = "/act/admin/savedeal?ts" + new Date().getTime();

          board.html("<img src='/images/loader01.gif' alt='Loading'/>");
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {            
              board.html(data.message);
              board.removeClass("warning error info");
              if (data.result == 0) {
                board.addClass("warning");
              } else if (data.result == -1) {
                board.addClass("error");
              } else if (data.result == 1) {
                board.addClass("info");
              }          

              if (data.redirect != null && data.redirect != undefined) {
                document.location = data.redirect;
              }
            },        

            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });

          return false;
        });            

        function edit(id) {
          var board = $("#board");      
          var params = "id=" + id;              

          var url = "/act/admin/deal?ts" + new Date().getTime();
          
          board.html("<img src='/images/loader01.gif' alt='Loading'/>");
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {            
              board.html(data.message);
              board.removeClass("warning error info");
              if (data.result == 0) {
                board.addClass("warning");
              } else if (data.result == -1) {
                board.addClass("error");
              } else if (data.result == 1) {
                board.addClass("info");
                var deal = data.deal;
                var redeem = data.redeem;
                var deal_date = data.deal_date;
                var deal_branch = data.deal_branch;
                var deal_state = data.deal_state;
                var merchant_branch = data.merchant_branch;
                
                if (deal != null && deal != undefined) {  
                  var input1 = $('<input>', {
                        id: 'deletePic1',
                        name: 'deletePic1',
                        type: 'button',
                        value: 'Delete',
                        click: function() {
                          var board = $("#board_image1");
                          var params = "id=" + deal.pht_id1; 
                          $("#image1_value").html("");
                          var url = "/act/admin/deletephoto?ts" + new Date().getTime();
                          $.ajax({
                            type: "post",
                            url: url,
                            data: params,
                            success: function(data, textStatus, jqXHR) {            
                              board.html(data.message);
                              board.removeClass("warning error info");
                              if (data.result == 0) {
                                board.addClass("warning");
                              } else if (data.result == -1) {
                                board.addClass("error");
                              } else if (data.result == 1) {
                                board.addClass("info");              
                              }          

                              if (data.redirect != null && data.redirect != undefined) {
                                document.location = data.redirect;
                              }
                            },        

                            error: function(objAJAXRequest, strError) {
                              if (strError == "timeout") {
                                console.info("Timeout");
                                board.addClass("error");
                                board.html("Connection timed out!");
                              } else {
                                board.addClass("error");
                                board.html("Unknown error!");
                              }
                            }

                          }).done(function(msg) {
                            console.info( "Response: " + msg);        
                          });
                        }
                   })
                  var input2 = $('<input>', {
                        id: 'deletePic2',
                        name: 'deletePic2',
                        type: 'button',
                        value: 'Delete',
                        click: function() {
                          var board = $("#board_image2");
                          var params = "id=" + deal.pht_id2; 
                          $("#image2_value").html("");
                          var url = "/act/admin/deletephoto?ts" + new Date().getTime();
                          $.ajax({
                            type: "post",
                            url: url,
                            data: params,
                            success: function(data, textStatus, jqXHR) {            
                              board.html(data.message);
                              board.removeClass("warning error info");
                              if (data.result == 0) {
                                board.addClass("warning");
                              } else if (data.result == -1) {
                                board.addClass("error");
                              } else if (data.result == 1) {
                                board.addClass("info");              
                              }          

                              if (data.redirect != null && data.redirect != undefined) {
                                document.location = data.redirect;
                              }
                            },        

                            error: function(objAJAXRequest, strError) {
                              if (strError == "timeout") {
                                console.info("Timeout");
                                board.addClass("error");
                                board.html("Connection timed out!");
                              } else {
                                board.addClass("error");
                                board.html("Unknown error!");
                              }
                            }

                          }).done(function(msg) {
                            console.info( "Response: " + msg);        
                          });
                        }
                   })
                  $("#image1_value").html("<img width='50' height='50' src='/photo?id="+deal.pht_id1+"&width=50&height=50&file=$filename'>");
                  $("#image1_value").append(input1);
                  $("#image2_value").html("<img width='50' height='50' src='/photo?id="+deal.pht_id2+"&width=50&height=50&file=$filename'>");
                  $("#image2_value").append(input2);
                  $("#title").val(deal.title);
                  $("#originalPr").val(deal.original_price);
                  $("#promotionPr").val(deal.promotion_price);
                  $("#minimumQ").val(deal.minimum_quantity);
                  $("#maximumQ").val(deal.maximum_quantity);
                  $("#categoryId").val(deal.dct_id);
                  $("#merchantId").val(deal.mct_id);
                  $("#startDate").val(deal_date.dealSatrtDate);
                  $("#endDate").val(deal_date.dealEndDate);
                  
                  $("#westCost").val(deal.west_shipping_cost);
                  $("#eastCost").val(deal.east_shipping_cost);
                  $("#redeemStartDate").val(redeem.redeemSatrtDate);
                  $("#redeemEndDate").val(redeem.redeemEndDate);
                  $('#pht_id1').val(deal.pht_id1);
                  $('#pht_id2').val(deal.pht_id2);
                  $("#inputId").val(deal.dea_id);
                  $("#confirmed").val(deal.confirmed);
                  $("#rdm_id").val(deal.rdm_id);
                  if(deal.short_description==null){
                    $('#shortDescription').html(tinyMCE.get('shortDescription').setContent(""));
                 }else{
                  $('#shortDescription').html(tinyMCE.get('shortDescription').setContent(deal.short_description));}
                 if(deal.description==null){
                  $('#description').html(tinyMCE.get('description').setContent(""));
                 }else{
                    $('#description').html(tinyMCE.get('description').setContent(deal.description));}
                  if(deal.terms==null){
                    $('#terms').html(tinyMCE.get('terms').setContent(""));
                  }else{
                     $('#terms').html(tinyMCE.get('terms').setContent(deal.terms));}
                  if(deal.highlights==null){
                    $('#highlight').html(tinyMCE.get('highlight').setContent(""));
                  }else{
                    $('#highlight').html(tinyMCE.get('highlight').setContent(deal.highlights));}
                  
                }
                if(merchant_branch != null && merchant_branch != undefined){
                  for (var i = 0; i < data.merchant_branch.length; i++) {
                    var newBranch = data.merchant_branch[i];
                    $("#branchId").append($('<option></option>').val(newBranch.brn_id).html(newBranch.name));
                  }
                  $("#branchId").val(deal_branch.brn_id);    
                  }
                  $("#stateId").val(deal_state.sta_id);
              }          

              if (data.redirect != null && data.redirect != undefined) {
                document.location = data.redirect;
              }
            },        

            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });
        }

        // ask for delete
        function askDelete(id) {        
          if (deleteDialog != null) {   
            deleteId = id;
            deleteDialog.dialog('open');
          }
        }

        // delete
        function doDelete(id) {
          var board = $("#board");      
          var params = "id=" + id;              

          var url = "/act/admin/deletedeal?ts" + new Date().getTime();

          board.html("<img src='/images/loader01.gif' alt='Loading'/>");
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {            
              board.html(data.message);
              board.removeClass("warning error info");
              if (data.result == 0) {
                board.addClass("warning");
              } else if (data.result == -1) {
                board.addClass("error");
              } else if (data.result == 1) {
                board.addClass("info");              
              }          

              if (data.redirect != null && data.redirect != undefined) {
                document.location = data.redirect;
              }
            },        

            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });
        }
        </script>
      </div>

      <br/>

      <div class="grid">
        <table id="grid" style="width: 100%">
          <thead>
            <tr>
              <th style="width: 20px">Row</th>
              <th style="width: 300px">Title</th>
              <th>Start Time</th> 
              <th>End Time</th>  
              <th>Promotion Price</th> 
              <th>Deal ID</th>
              <th style="width: 70px">Action</th>
            </tr>
          </thead>
          <tbody>

          </tbody>        
        </table>
        {literal}
        <script type="text/javascript">
        var deleteDialog = null;
        var deleteId = null;      
        var gridUrl = "/act/admin/deals";      
        $(document).ready(
          function() {          
            tblCountries = $('#grid').dataTable( {                  
              "bProcessing": true,
              "bJQueryUI": true,
              "sAjaxSource": gridUrl,            
              fnDrawCallback: function() {              
                $(".edit").button({ text: false, icons: {primary:'ui-icon-pencil'} });              
                $(".edit").click(function() {
                  edit($(this).attr('rel'));
                });

                $(".delete").button({ text: false, icons: {primary:'ui-icon-trash'} });
                $(".delete").click(function() {
                  askDelete($(this).attr('rel'));
                });
              }                    
          }); // dataTable        

          deleteDialog = $("#deleteDialog").dialog({ autoOpen: false, show: { effect: 'drop', direction: "up" }, title: deleteDialogCaption });
          $("#btnDeleteCancel").click(function(){
            deleteDialog.dialog('close');
          });
          $("#btnDeleteOK").click(function(){
            doDelete(deleteId);
            deleteDialog.dialog('close');
          });
          
          $("#merchantId").change(function() {
            var merchantId = $(this).val();
            var select = "--Select--";
            // remove current items
            $("#branchId option").remove();
            $("#branchId").append($('<option></option>').val(0).html(select));

            // fetch new items          

            $.getJSON('/act/admin/branchsbymerchant?id=' + merchantId,  
         
              function(data) {
                
                if (data.status != undefined && data.status == 1) {
                  
                  var newBranchs = data.list;
                  for (var i = 0; i < newBranchs.length; i++) {
                    var newBranch = newBranchs[i];
                    $("#branchId").append($('<option></option>').val(newBranch.brn_id).html(newBranch.name));
                  }
                }
            });
          });
        });       
        </script>
        {/literal}
        <br/>
        <br/>

        <div id="deleteDialog">
          {#confirmDelete#}
          <br/>
          <br/>
          <div class="buttons">
            <button class="button" id="btnDeleteOK">{#ok#}</button>
            <button class="button" id="btnDeleteCancel">{#cancel#}</button>        
          </div>
        </div>
      </div>
    {else}
      {include file="admin/accessdenied.tpl"}
    {/if}
  {else}
    {include file="admin/notloggedin.tpl"}
  {/if}
</div>