<!-- User registration -->
<?php

include_once 'init.php';
require_once '../../libs/connectionmanager.class.php';
require_once '../../libs/securitymanager.class.php';
require_once '../../libs/sessionmanager.class.php';
require_once '../../libs/entitymanager.class.php';
require_once '../../libs/utils.class.php';
require_once '../../libs/cookiemanager.class.php';

ob_start();
$message = "";
$status = 0;

$username = Utils::stringPost("username");
$email = Utils::stringPost("email");
$password = Utils::stringPost("password");
$confirmPassword = Utils::stringPost("confirmPassword");
$recaptchaChallengeField = Utils::stringPost("recaptcha_challenge_field");
$recaptchaResponseField = Utils::stringPost("recaptcha_response_field");
$main_userId = Utils::stringPost("main_username");

$ip = Utils::getRealIP();

$captchaResult = SecurityManager::checkCaptcha($recaptchaChallengeField, $recaptchaResponseField, $ip);

if ($captchaResult != true) {
  $status = -1;
  $message = $smarty->getConfigVars("wrongCaptcha");
} else if ($username == NULL || strlen(trim($username)) <= 0) {
  $message = $smarty->getConfigVars("enterUsername");
} else if (!Utils::isValidUsername($username)) {
  $message = $smarty->getConfigVars("invalidUsername");
}else if(!Utils::check_duplicated_user($username)){
  $message = $smarty->getConfigVars("duplicatedUsername");
} else if ($email == NULL || strlen(trim($email)) <= 0) {
  $message = $smarty->getConfigVars("enterEmailAddress");
} else if (!Utils::isValidEmail($email)) {
  $message = $smarty->getConfigVars("invalidEmailAddress");
}else if(!Utils::check_duplicated_email($email)){
  $message = $smarty->getConfigVars("duplicatedEmail");
} else if ($password == null || strlen($password) < 8) {
  $message = $smarty->getConfigVars("enterPassword");
} else if ($confirmPassword != $password) {
  $message = $smarty->getConfigVars("confirmPasswordNotMatch");
} else {
  $pdo = null;
  try {
    
    $pdo = ConnectionManager::generatePDO();
    $pdo->beginTransaction();
    $role = 0; // normal user

    $hashedPassword = SecurityManager::saltedHash($password);
    $userResult = EntityManager::insertUser($pdo, $email, $username, $hashedPassword, $role);
    $userId = $userResult['id'];
    
    if ($userId > 0) {
      $now = new DateTime();
      $nowTimestamp = $now->getTimestamp();
      $profileResult = EntityManager::insertProfile($pdo, $userId, "", "", $nowTimestamp);
      if ($profileResult['id'] > 0) {
        $token = SecurityManager::sha256($username);
        $tokenResult = EntityManager::insertRegistrationToken($pdo, $userId, $token, $nowTimestamp);
        if ($tokenResult['id'] > 0) {
           if($main_userId != NULL){
            $main_userId = Utils::stringPost("main_username");
            $usrResult = EntityManager::readUser($pdo, $main_userId);
            if($userResult!=NULL){
              $usr_id = $usrResult["usr_id"];
              $link = "http://businessdeal.my/page/activation/$token/$usr_id";
            }else{
              $link = "http://businessdeal.my/page/activation/$token";
            }           
          }else{
            $link = "http://businessdeal.my/page/activation/$token";
          }
          $smarty->assign("link", $link);
          $body = $smarty->fetch("email/registrationEmail.tpl");
          if (Utils::sendEmailFromGmail($email, "noreply@businessdeal.my", "BusinessDeal", "Registration", $body)) {
            $pdo->commit();
            $status = 1;
            $message = $smarty->getConfigVars("youHaveSuccessfullyRegistered");
          } else {
            $pdo->rollBack();
            $status = -1;
            $message = $smarty->getConfigVars("registrationFailedBecauseOfSMTP");
          }
        }
      }
    } else {
      $pdo->rollBack();
      $status = -1;
      $message = $smarty->getConfigVars("unknownProblem");
    }
  } catch (PDOException $e) {
    $status = -1;
    $message = "Error: " + $e->getMessage();
    if ($pdo != null) {
      $pdo->rollBack();
    }
  }
}

$result = array("result" => $status, "message" => $message);
$result = json_encode($result);

ob_clean();
ob_start();

echo $result;
ob_flush();
?>