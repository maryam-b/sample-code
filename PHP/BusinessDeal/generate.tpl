<!-- generate and send/resend voucher to cunstomer - admin panel -->
<div id="mainContent">
  {if $loggedIn}
    {if $loggedInUser.role ge 5}
      <div id="pageTitle">{#voucher#}</div>
      <div class="form">
        <form id="frmNewVoucher">
          <table class="form">
            <tr>
              <td class="caption">{#dealId#}:</td>
              <td class="data">
                <input type="text" name="dealId" id="dealId" maxlength="100"/> e.g {#dealIdExample#}
              </td>
            </tr>
            <tr>
              <td class="buttons" colspan="2">
                <input type="hidden" name="id" value="0" id="inputId"/>
                <input type="submit" class="button" value="{#show#}" name="ok" id="ok"/>
                <input type="reset" class="button" value="{#new#}" id="new" />
              </td>          
            </tr>
          </table>
        </form> 
        {literal}
        <script type="text/javascript">

        var tblVouchers = null;
        focus('name');
        
        $('#new').click(function() {
            $("#inputId").val(0);
            $("#board").html("");
            focus('dealId');
        });
        
        $('#frmNewVoucher').submit(function() {   
        
          $("#editTrans").css({'visibility':'collapse'});
          $("#sendVoucher").css({'visibility':'visible'});
          $("#resendVoucher").css({'visibility':'visible'});
          $("#secondCaption").css({'visibility':'collapse'});
          $("#depositCaption").css({'visibility':'collapse'});
          $("#deposit").css({'visibility':'collapse'});
          $("#secondRef").css({'visibility':'collapse'});
          $("#secondTrans").css({'visibility':'collapse'});
          $("#secondResponTime").css({'visibility':'collapse'});
          $("#secondAmount").css({'visibility':'collapse'});
          $("#firstAmount").css({'visibility':'collapse'}); 
          var params = $("#frmNewVoucher").serialize();              

          var url = "/act/admin/voucher?ts" + new Date().getTime();
          var myvalue=$('#dealId').val();
	  var gridUrl = "/act/admin/voucher";
          $("#offerId").val(myvalue);
          $("#resendofferId").val(myvalue);
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function() {          
              tblVouchers = $('#grid').dataTable( { 
              "bDestroy": true,                 
              "bProcessing": true,
              "bJQueryUI": true,
              "sAjaxSource": gridUrl,
              "fnServerParams": function ( aoData ) {
              aoData.push( { "name": "dealId", "value": myvalue } );
          },    
             fnDrawCallback: function() {              
                $(".edit").button({ text: false, icons: {primary:'ui-icon-pencil'} });              
                $(".edit").click(function() {
                  edit($(this).attr('rel'));
                });
              }               
                                            
          }); // dataTable  ,        
},
            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });

          return false;
        });            

        </script>
         {/literal}
      </div>
      <br/>
      <div id="sendVoucher" class="form" style="visibility: collapse">
      <form id="frmSendVoucher">
          <br/>
          <div class="buttons">
             <table class="form">
              <tr>
              <td class="caption">{#startDate#}:</td>
              <td class="data">
                <input type="text" name="startDate" id="startDate" /> 
                <span>{#exampleDate#}</span>
              </td>
              </tr>
              <tr>
              <td class="caption">{#endDate#}:</td>
              <td class="data">
                <input type="text" name="endDate" id="endDate" />
                <span>{#exampleDate#}</span>
              </td>
            </tr>
            </table>
            <div id="board"></div>           
            <input type="hidden" name="offerId" id="offerId" value="0" >
            <input type="submit" class="button" value="{#generateSend#}" name=send" id="send"/>                  
          </div>
      </form>
       {literal}
         <script type="text/javascript">
          $('#frmSendVoucher').submit(function() {      
          var board = $("#board");      
          var params = $("#frmSendVoucher").serialize();              

          var url = "/act/admin/sendvoucher?ts" + new Date().getTime();

          board.html("<img src='/images/loader01.gif' alt='Loading'/>");
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {            
              board.html(data.message);
              board.removeClass("warning error info");
              if (data.result == 0) {
                board.addClass("warning");
              } else if (data.result == -1) {
                board.addClass("error");
              } else if (data.result == 1) {
                board.addClass("info");
                //tblVouchers.fnReloadAjax();
              }          

              if (data.redirect != null && data.redirect != undefined) {
                document.location = data.redirect;
              }
            },        

            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });

          return false;
        });     
        </script>
       {/literal}
      </div>
      <br/>
      <div id="resendVoucher" class="form" style="visibility: collapse">
        <br/>
      <form id="frmResendVoucher">
        <br/>
          <div class="buttons">
            <div id="resendBoard"></div>
            <input type="hidden" name="limit" id="limit" value="0" >
            <input type="hidden" name="resendofferId" id="resendofferId" value="0" >
            <input type="submit" class="button" value="{#resend#}" name=resend" id="resend"/>&nbsp;<span>{#resendMessage#}</span>    
          </div>
      </form>
          {literal}
         <script type="text/javascript">
          $('#frmResendVoucher').submit(function() {      
          var board = $("#resendBoard");      
          var params = $("#frmResendVoucher").serialize();              

          var url = "/act/admin/resendvoucher?ts" + new Date().getTime();

          board.html("<img src='/images/loader01.gif' alt='Loading'/>");
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {            
              board.html(data.message);
              board.removeClass("warning error info");
              if (data.result == 0) {
                board.addClass("warning");
              } else if (data.result == -1) {
                board.addClass("error");
              } else if (data.result == 1) {
                board.addClass("info");
                //tblVouchers.fnReloadAjax();
              }          

              if (data.redirect != null && data.redirect != undefined) {
                document.location = data.redirect;
              }
            },        

            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });

          return false;
        });     
        </script>
       {/literal}      
      </div>
       <br>
      <div id="editTrans" class="form" style="visibility: collapse">
        <form id="frmEditTrans">
          <table class="form">
            <tr>
              <td class="caption">{#username#}:</td>
              <td class="data">
                <input type="text" name="username" id="username" disabled="disabled"/> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#firstName#}&nbsp;{#lastName#}:</td>
              <td class="data">
                <input type="text" name="family" id="family" disabled="disabled"/>
              </td>
            </tr>
            <tr>
              <td class="caption">{#email#}:</td>
              <td class="data">
                <input type="text" name="email" id="email"  disabled="disabled"/>
              </td>
            </tr>
            <tr>
              <td class="caption">{#title#}:</td>
              <td class="data">
                <input type="text" name="title" id="title" maxlength="500" style="width: 500px" disabled="disabled" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#quantity#}:</td>
              <td class="data">
                <input type="text" name="quantity" id="quantity" disabled="disabled" /> 
              </td>
            </tr>
            <tr >
              <td class="caption" id="depositCaption" style="visibility: collapse">{#deposit#}:</td>
              <td class="data" id="deposit" style="visibility: collapse">
                <input type="text" name="depositAmount" id="depositAmount"  disabled="disabled" /> 
              </td>
            </tr>
            <tr id="firstAmount">
              <td class="caption">{#total#}:</td>
              <td class="data">
                <input type="text" name="amount" id="amount" disabled="disabled" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#token#}:</td>
              <td class="data">
                <input type="text" name="token" id="token" disabled="disabled" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#responseDate#}:</td>
              <td class="data">
                <input type="text" name="responeDate" id="responeDate" disabled="disabled" /> 
              </td>
            </tr>           
             <tr>
              <td class="caption">{#refNo#}:</td>
              <td class="data">
                <input type="text" name="refNum" id="refNum" disabled="disabled" /> 
              </td>
            </tr>
            
            <tr>
              <td class="caption">{#transactionId#}:</td>
              <td class="data">
                <input type="text" name="transactionId" id="transactionId" disabled="disabled"/>
              </td>
            </tr>
            <tr>
              <td class="caption">{#voucherCode#}:</td>
              <td class="data">
                <input type="text" name="vocher_num" id="vocher_num" disabled="disabled" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#securityCode#}:</td>
              <td class="data">
                <input type="text" name="securityCode" id="securityCode" disabled="disabled" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#voucherCount#}:</td>
              <td class="data">
                <input type="text" name="voucherCount" id="voucherCount" disabled="disabled" /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#validFrom#}:</td>
              <td class="data">
                <input type="text" name="from_date" id="from_date"  /> 
              </td>
            </tr>
            <tr>
              <td class="caption">{#validTo#}:</td>
              <td class="data">
                <input type="text" name="to_date" id="to_date"  /> 
              </td>
            </tr>
            <tr id="secondCaption" style="visibility: collapse">
              <td class="caption">{#secondProcess#} </td>
              <td><br><br></td>
            </tr>
            <tr id="secondRef" style="visibility: collapse">
              <td class="caption" >{#refNo#}:</td>
              <td class="data">
                <input type="text" name="secondRefNum" id="secondRefNum" disabled="disabled"/> 
              </td>
            </tr>
            <tr id="secondTrans" style="visibility: collapse">
              <td class="caption" >{#transactionId#}:</td>
              <td class="data">
                <input type="text" name="secondTrsId" id="secondTrsId" disabled="disabled"/> 
              </td>
            </tr>
            <tr id="secondResponTime" style="visibility: collapse">
              <td class="caption" >{#responseDate#}:</td>
              <td class="data">
                <input type="text" name="secondRespone" id="secondRespone" disabled="disabled"/> 
              </td>
            </tr>
            <tr id="secondAmount" style="visibility: collapse">
              <td class="caption" >{#total#}:</td>
              <td class="data">
                <input type="text" name="secondPurAmount" id="secondPurAmount" disabled="disabled"/> 
              </td>
            </tr>
            <tr>
              <td class="buttons" colspan="2">
                <input type="hidden" name="vch_id" value="0" id="vch_id"/>
                <input type="hidden" name="dea_id" value="0" id="dea_id"/>
                <input type="hidden" name="count" value="0" id="count"/>
                <input type="hidden" name="email_address" value="0" id="email_address"/>
                <input type="submit" class="button" value="{#send#}" name="ok" id="ok"/>
              </td>          
            </tr>
            <tr>
              <td colspan="2">
                <div id="oneboard"></div>
              </td>          
            </tr>
          </table>
        </form>
      </div>
      {literal}
      <script type="text/javascript">
          
          $('#from_date').datetimepicker({
                  controlType: 'select'               
            });
              
          $('#to_date').datetimepicker({
                  controlType: 'select'               
            });
              
              
         $('#frmEditTrans').submit(function() {      
          var board = $("#oneboard");      
          var params = $("#frmEditTrans").serialize();              
  
          var url = "/act/admin/sendonevoucher?ts" + new Date().getTime();

          board.html("<img src='/images/loader01.gif' alt='Loading'/>");
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {            
              board.html(data.message);
              board.removeClass("warning error info");
              if (data.result == 0) {
                board.addClass("warning");
              } else if (data.result == -1) {
                board.addClass("error");
              } else if (data.result == 1) {
                board.addClass("info");
                //tblCountries.fnReloadAjax();
              }          

              if (data.redirect != null && data.redirect != undefined) {
                document.location = data.redirect;
              }
            },        

            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });

          return false;
        });     
          
          
         function edit(id) {
           
          $("#sendVoucher").css({'visibility':'collapse'});
          $("#resendVoucher").css({'visibility':'collapse'});
          var board = $("#oneboard");      
          var params = "id=" + id ; 
            
          $("#editTrans").css({'visibility':'visible'});
          var url = "/act/admin/voucherinfo?ts" + new Date().getTime();

          board.html("<img src='/images/loader01.gif' alt='Loading'/>");
          $.ajax({
            type: "post",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {            
              board.html(data.message);
              board.removeClass("warning error info");
              if (data.result == 0) {
                board.addClass("warning");
              } else if (data.result == -1) {
                board.addClass("error");
              } else if (data.result == 1) {
                board.addClass("info");
                var voucherInfo = data.voucher[0];   
                var family = data.family;
                var title = data.title;
                var voucher = data.vouchers;
                var parentId = voucherInfo.parent_id;
                var parentInfo = data.parentInfo;
                if (voucherInfo != null && voucherInfo != undefined) {                
                  $("#username").val(voucherInfo.username);
                  $("#family").val(family);
                  $("#email").val(voucherInfo.email);
                  $("#email_address").val(voucherInfo.email);                 
                  $("#title").val(title);
                  $("#quantity").val(voucherInfo.quantity);
                  $("#new_qty").val(voucherInfo.quantity);
                    if(parentId == 1){                 
                      $("#deposit").css({"visibility":"visible"});
                      $("#secondResponTime").css({"visibility":"visible"});  
                      $("#secondRef").css({"visibility":"visible"});
                      $("#secondTrans").css({"visibility":"visible"});
                      $("#depositCaption").css({"visibility":"visible"});
                      $("#secondCaption").css({"visibility":"visible"});
                      $("#secondAmount").css({"visibility":"visible"});
                      $("#firstAmount").css({"visibility":"collapse"});
                      $("#depositAmount").val(voucherInfo.amount);
                      $("#secondPurAmount").val(parentInfo.amount);
                      $("#secondTrsId").val(parentInfo.transaction_id);
                      $("#secondRefNum").val(parentInfo.ref_no);
                      $("#secondRespone").val(parentInfo.respone_time);
                   }else if(parentId== 0){
                     $("#deposit").css({"visibility":"collapse"});
                     $("#depositCaption").css({"visibility":"collapse"});
                     $("#secondResponTime").css({"visibility":"collapse"});
                     $("#secondRef").css({"visibility":"collapse"});
                     $("#secondTrans").css({"visibility":"collapse"});
                     $("#secondCaption").css({"visibility":"collapse"});
                     $("#secondAmount").css({"visibility":"collapse"});
                     $("#firstAmount").css({"visibility":"visible"});
                     $("#amount").val(voucherInfo.amount);
                    }
                    
                  $("#token").val(voucherInfo.token);  
                  $("#responeDate").val(voucherInfo.respone_time);
                  $("#refNum").val(voucherInfo.ref_no);
                  $("#transactionId").val(voucherInfo.transaction_id);
                  $("#dea_id").val(voucherInfo.dea_id);
                  
                  if(voucherInfo.security_code != null && voucherInfo.voucher_number != null ){
                      $("#vocher_num").val(voucherInfo.voucher_number);
                      $("#securityCode").val(voucherInfo.security_code);
                      $("#voucherCount").val(voucherInfo.count);
                      $("#vch_id").val(voucherInfo.vch_id);
                      $("#count").val(voucherInfo.count);
                  }else{
                     $("#vocher_num").val(voucher.voucher_number);
                     $("#securityCode").val(voucher.security_code);
                     $("#voucherCount").val(voucher.count);
                     $("#vch_id").val(voucher.vch_id);
                     $("#count").val(voucher.count);
                  }
                                   
                  $("#from_date").val(voucherInfo.from_date);
                  if(voucherInfo.from_date!= null){
                    $("#from_date").attr("disabled", "disabled"); 
                   }else{
                     $("#from_date").removeAttr("disabled");
                    }
                  $("#to_date").val(voucherInfo.to_date);    
                  if(voucherInfo.to_date!= null){
                    $("#to_date").attr("disabled", "disabled"); 
                   }else{
                     $("#to_date").removeAttr("disabled");
                    }
            }
           }          

              if (data.redirect != null && data.redirect != undefined) {
                document.location = data.redirect;
              }
            },        

            error: function(objAJAXRequest, strError) {
              if (strError == "timeout") {
                console.info("Timeout");
                board.addClass("error");
                board.html("Connection timed out!");
              } else {
                board.addClass("error");
                board.html("Unknown error!");
              }
            }

          }).done(function(msg) {
            console.info( "Response: " + msg);        
          });
        }
        </script>
      {/literal}
      <br/>
      <div class="grid">
        <table id="grid" style="width: 100%">
          <thead>
            <tr>
              <th style="width: 20px">Row</th>
              <th>First Name</th>
              <th>Last Name</th>  
              <th>Username</th>
              <th>Email</th>
              <th>Transaction Date</th>
              <th>Vouchers Count</th>
              <th style="width: 70px">Action</th>
            </tr>
          </thead>
          <tbody>

          </tbody>        
        </table>
        {literal}
        <script type="text/javascript">
        var deleteDialog = null;
        var deleteId = null;      
        var gridUrl = "/act/admin/voucher";      
        $(document).ready(
          function() {          
            tblVouchers = $('#grid').dataTable( {                  
              "bProcessing": true,
              "bJQueryUI": true,
              "sAjaxSource": gridUrl,            
              fnDrawCallback: function() {              
                $(".edit").button({ text: false, icons: {primary:'ui-icon-pencil'} });              
                $(".edit").click(function() {
                  edit($(this).attr('rel'));
                });
              }                    
          }); // dataTable  
            $('#startDate').datetimepicker({
                  controlType: 'select'
                  
            });
            $('#endDate').datetimepicker({
                  controlType: 'select'
                  
            });
        });       
        </script>
        {/literal}
        <br/>
        <br/>
      </div>
    {else}
      {include file="admin/accessdenied.tpl"}
    {/if}
  {else}
    {include file="admin/notloggedin.tpl"}
  {/if}
</div>