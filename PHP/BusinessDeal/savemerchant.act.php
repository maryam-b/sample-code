<!-- This action file handles saving merchant that request from Add merchant-Admin panel -->
<?php

include_once 'init.php';
require_once '../../libs/connectionmanager.class.php';
require_once '../../libs/securitymanager.class.php';
require_once '../../libs/sessionmanager.class.php';
require_once '../../libs/entitymanager.class.php';
require_once '../../libs/utils.class.php';

ob_start();

$message = "";
$status = 0;

if (SessionManager::isLoggedIn()) {

  $companyName = Utils::stringPost("companyName");
  $username = Utils::stringPost("username");
  $password = Utils::stringPost("password");
  $accountEmail = Utils::stringPost("accountEmail");
  $positionTitle = Utils::stringPost("positionTitle");
  $merchant_status = Utils::stringPost("status");
  $mch_branchName = Utils::stringPost("mch_branchName");
  $mch_country = Utils::integerPost("mch_country");
  $id = Utils::integerPost("id");

  $companyName = Utils::htmlEntities($companyName);
  $username = Utils::htmlEntities($username);
  $positionTitle = Utils::htmlEntities($positionTitle);

  if ($companyName == NULL || strlen(trim($companyName)) <= 0) {
    $message = $smarty->getConfigVars("enterNameOfCompany");
  } else if ($username == NULL || strlen(trim($username)) <= 0) {
    $message = $smarty->getConfigVars("enterUsername");
  } else if ($id <= 0 && ($password == NULL || strlen(trim($password)) < 8)) {
    $message = $smarty->getConfigVars("enterPassword");
  }else if ($id > 0 && $password != NULL && strlen(trim($password)) < 8) {
    $message = $smarty->getConfigVars("enterPassword");
  } else if (!Utils::isValidEmail($accountEmail)) {
    $message = $smarty->getConfigVars("invalidEmailAddress");
  } else if (!Utils::isValidUsername($username)) {
    $message = $smarty->getConfigVars("invalidUsername");
  } else if ($merchant_status == 0) {
    $message = $smarty->getConfigVars("selectStatus");
  }else if ($mch_branchName == NULL) {
    $message = $smarty->getConfigVars("enterBranch");
  }else if ($mch_country == 0) {
    $message = $smarty->getConfigVars("enterBranch");
  }else {
    $pdo = null;
    
    try {
      $pdo = ConnectionManager::generatePDO();

      $companyName = trim($companyName);
      $positionTitle = trim($positionTitle);

      $branchResult = null;
      $userResult = null;

      $pdo->beginTransaction();

      if ($id <= 0) {
        $password = SecurityManager::saltedHash($password);
        $branchResult = EntityManager::insertMerchant($pdo, $companyName, $positionTitle, NULL);
        $merchantId = $branchResult['id'];
        $id = $merchantId;
        if ($branchResult['rows'] == 1 && $merchantId > 0) {
          $userResult = EntityManager::insertUser($pdo, $accountEmail, $username, $password, 3);
          
          $addressId = $userResult['id'];
          if ($userResult['rows'] == 1 && $addressId > 0) {

            $rows = EntityManager::update($pdo, "merchant", "mct", $merchantId, "usr_id", $addressId);
            if($merchant_status == 1){
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'active', $merchant_status);
            }elseif($merchant_status == 2){
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'active', 1);
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'suspended', 1);
            }elseif($merchant_status == 3){
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'active', 0);
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'suspended', 0);
            }
            if ($rows == 1) {
              $status = 1;
              $message = $smarty->getConfigVars("savedSuccessfully");
              $pdo->commit();
            } else {
              $status = -1;
              $message = $smarty->getConfigVars("unknownProblem");
              $pdo->rollBack();
            }
          } else {
            $status = -1;
            $message = $smarty->getConfigVars("merchantUserInsertFailed");
            $message .= " " . $smarty->getConfigVars("checkDuplication");
            $pdo->rollBack();
          }
        } else {
          $status = -1;
          $message = $message = $smarty->getConfigVars("merchantBasicsInsertFailed");
          $pdo->rollBack();
        }
      } else {
        $branch = EntityManager::readMerchant($pdo, $id);
        if ($branch != null && $branch['mct_id'] > 0) {
          $addressId = $branch['usr_id'];
          if($password != NULL ){
             $password = SecurityManager::saltedHash($password);
             EntityManager::update($pdo, 'user', 'usr', $addressId, 'password', $password);
            }
          EntityManager::updateMerchant($pdo, $id, $companyName, $positionTitle, $addressId);
           if($merchant_status == 1){
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'active', $merchant_status);
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'suspended', 0);
           }elseif($merchant_status == 2){
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'suspended', 1);
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'active', 1);
           }elseif($merchant_status == 3){
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'suspended', 0);
              EntityManager::update($pdo, 'user', 'usr', $addressId, 'active', 0);
           } 
          $status = 1;          
          $message = $smarty->getConfigVars("savedSuccessfully");
          $pdo->commit();
        }
      }
    } catch (PDOException $e) {
      if ($pdo != null) {
        $pdo->rollBack();
      }
      $status = -1;
      $message = "Error: " + $e->getMessage();
    }
  } // else

  $result = json_encode(array("result" => $status, "message" => $message, "id" => $id));
} else {
  $message = $smarty->getConfigVars("sessionExpired");
  $status = -1;
  $result = json_encode(array("result" => $status, "message" => $message, "redirect" => "/admin"));
}



ob_clean();
ob_start();

echo $result;
ob_flush();
?>
