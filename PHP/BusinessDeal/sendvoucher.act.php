<?php

include_once 'init.php';
require_once '../../libs/connectionmanager.class.php';
require_once '../../libs/securitymanager.class.php';
require_once '../../libs/sessionmanager.class.php';
require_once '../../libs/entitymanager.class.php';
require_once '../../libs/paymentmanager.class.php';
require_once '../../libs/utils.class.php';

ob_start();

$message = "";
$status = 0;

if (SessionManager::isLoggedIn()) {

  $pdo = ConnectionManager::generatePDO();
 if(!$id = Utils::integerPost("offerId"))
 	$id = Utils::integerGet("offerId");
  $startDate = Utils::stringPost("startDate");
  $startDate = Utils::setTimeStamp($startDate);
  $endDate = Utils::stringPost("endDate");
  $endDate = Utils::setTimeStamp($endDate);
  if($startDate == NULL || $endDate == NULL){
      $now = new DateTime();
      $startDate = $now->getTimestamp();
      $endDate = $startDate+5270400;
    
  }
  $deal_result = EntityManager::readDeal($pdo, $id);
  $mct_id = $deal_result["mct_id"];
  $merchant_info = EntityManager::readMerchant($pdo, $mct_id);
  $deal_branch = EntityManager::readDealBranch($pdo, $id); 
  $brn_id = $deal_branch["brn_id"];
  $branch_info = EntityManager::readBranch($pdo, $brn_id);
  $adrs_id = $branch_info["adr_id"];
  $branch_adrs = EntityManager::readAddress($pdo, $adrs_id);
  //print_r($branch_adrs);
  $smarty->assign("deal_result", $deal_result);
  $smarty->assign("merchant_info", $merchant_info);
  $smarty->assign("branch_info", $branch_info);
  $smarty->assign("branch_adrs", $branch_adrs);
 
if ($id == NULL || $id == 0) {
    $message = $smarty->getConfigVars("enterOfferId");
  } else {
    try {
      

      $send_voucher = null;
      if ($id <= 0) {
        $message = $smarty->getConfigVars("unknownProblem");
      } else {
        $send_voucher = EntityManager::read_send_voucher($pdo, $id);
        //print_r($send_voucher);
      }

      if ($send_voucher == null ) {
         $message = $smarty->getConfigVars("noNewPurchase");
     	 $status = 1;
     }else{
        foreach($send_voucher as $send){
          if($send["vch_id"]>0 && $send["count"]==0){

            $vch_id = $send["vch_id"];
            $voucher_info = EntityManager::read_voucher($pdo, $vch_id);
            $pur_id = $voucher_info['pur_id'];
            $purchase_info = PaymentManager::readPurchase($pdo, NULL, $pur_id);
            $count = $voucher_info["count"]+1;
            $email = $send['email'];
            $smarty->assign("voucher_info", $voucher_info);   
            $smarty->assign("purchase_info", $purchase_info);
            $body = $smarty->fetch("email/voucherEmail.tpl");
            if (Utils::sendEmailFromGmail($email, "noreply@businessdeal.my", "BusinessDeal", "Your Voucher is ready", $body)) {              
             $update_voucher = EntityManager::update($pdo, "vouchers", "vch", $vch_id, "count", $count) ;
             if ($update_voucher != null && is_array($update_voucher)) {
               $update_voucher = $update_voucher['rows'];
            }
            if ($update_voucher != null && $update_voucher == 1) {
              $message = $smarty->getConfigVars("mailSend");
              $status = 1;
            }else {
            $message = $smarty->getConfigVars("unknownProblem");
            $status = -1;
            }           
          } 
          }else{
            
            $security_code = EntityManager::generate_voucher_password();
            $voucher_number = EntityManager::generate_voucher_number();

            //$now = new DateTime();
           // $from_date = $now->getTimestamp();
           // $to_date = $from_date+5184000;
            $count = 0;
            $pur_id = $send['pur_id'];
          //  $vouchers = EntityManager::insert_voucher($pdo, $voucher_number, $security_code, $from_date, $to_date, $pur_id, $count);
            $vouchers = EntityManager::insert_voucher($pdo, $voucher_number, $security_code, $pur_id, $count, $startDate, $endDate);
            if ($vouchers != null && is_array($vouchers)) {
               $vch_id = $vouchers["id"];
               $vouchers = $vouchers['rows'];
            }
            if ($vouchers != null && $vouchers == 1) {
              $voucher_info = EntityManager::read_voucher($pdo, $vch_id);
              //print_r($voucher_info);
              $count = $voucher_info["count"]+1;
              $pur_id = $voucher_info['pur_id'];
              $purchase_info = PaymentManager::readPurchase($pdo, NULL, $pur_id);
              $smarty->assign("voucher_info", $voucher_info);
              $smarty->assign("purchase_info", $purchase_info);
              $body = $smarty->fetch("email/voucherEmail.tpl");
              $email = $send['email'];
              if (Utils::sendEmailFromGmail($email, "noreply@businessdeal.my", "BusinessDeal", "Your Voucher is ready", $body)) {              
               $update_voucher = EntityManager::update($pdo, "vouchers", "vch", $vch_id, "count", $count) ;
             if ($update_voucher != null && is_array($update_voucher)) {
               $update_voucher = $update_voucher['rows'];
            }
            if ($update_voucher != null && $update_voucher == 1) {
              $message = $smarty->getConfigVars("mailSend");
              $status = 1;
            }else {
            $message = $smarty->getConfigVars("unknownProblem");
            $status = -1;
            }           
          } 
            }else{
            $message = $smarty->getConfigVars("unknownProblem");
            if ($id <= 0) {
              $message .= " " . $smarty->getConfigVars("checkDuplication");
            }
            $status = -1;
            }            
          }
        }
        header("Location: ".$_SERVER['php-self']."?offerId=$id");
      } 
    } catch (PDOException $e) {
      $status = -1;
      $message = "Error: " + $e->getMessage();
    }
  }
  $result = json_encode(array("result" => $status, "message" => $message));
} else {
  $message = $smarty->getConfigVars("sessionExpired");
  $status = -1;
  $result = json_encode(array("result" => $status, "message" => $message, "redirect" => "/admin"));
}



ob_clean();
ob_start();

echo $result;
ob_flush();
?>