<!-- once deal created for merchant and publich, merchant can see the status of deal, this action handles that -->
<?php

include_once 'init.php';
require_once '../../libs/connectionmanager.class.php';
require_once '../../libs/sessionmanager.class.php';
require_once '../../libs/entitymanager.class.php';
require_once '../../libs/utils.class.php';

ob_start();

if (SessionManager::isLoggedIn()) {
  
  $pdo = ConnectionManager::generatePDO();
  
  if(isset($_SESSION["loggedInUser"])){
    $user = $_SESSION["loggedInUser"];
    $usr_id = $user['usr_id'];
    $condition = " m.usr_id =".$usr_id;
    $merchant_info = EntityManager::selectMerchants($pdo, $condition);
    //print_r($merchant_info);
    $mct_id = $merchant_info[0]['mct_id'];

  }

  $edit = $smarty->getConfigVars("edit");
  $now = new DateTime();
  $nowDate = date("Y-m-d H:i:s");
  
  
  $deal_purchase = EntityManager::readDealByPurchase($pdo,$mct_id);
  //print_r($deal_purchase);
  $result = array();
  $rows = array();

  if ($deal_purchase != null) {
    $i = 0;
    foreach ($deal_purchase as $deal) {
      $i++;

      $row = array();
      
      $id = $deal['dea_id'];
      $row[] = $i; 
      
      $row[] = $deal['title'];
      $row[] = $deal['start_time']; 
      $row[] = $deal['end_time']; 
      $row[] = $deal['promotion_price'];
      $row[] = $deal['name'];
      $end_time = $deal['end_time'];
      if($end_time >= $nowDate){
        $row[] = 'Ongoing';
      }else{
        $row[] = 'Closed';     
      }
      if($deal['purchase'] != NULL){
      $row[] = $deal['purchase']; }
      else{
        $row[] = 0;
      }
      if($end_time <= $nowDate && $deal['purchase'] == NULL ){
        $row[] = "Failed Deal";
      }elseif($deal['purchase']>0 && $deal['purchase']<$deal['maximum_quantity'] && $end_time <= $nowDate){
        $row[] = "Successful Deal";
      }elseif ($deal['purchase'] != NULL && $deal['purchase']==$deal['maximum_quantity']) {
        $row[] = "Sold Out";
      }elseif ($end_time >= $nowDate) {
        $row[] = "Current Deal";
      }
      if($deal['purchase'] != NULL){
        $pur_id = $deal['pur_id'];
        $voucher_info = EntityManager::read_voucher($pdo, NULL, $pur_id);
        if($voucher_info != NULL){
       
        $row[] = "<a  href='/admin/mchvoucherreport/$id'>Vouchers</a>";}
        else{
          $row[] = "-";
        }
      }else{
        $row[] = "-";
      }
     $row[] = "<button title='$edit' class='button edit' rel='$id'>$edit</button>";
      $rows[] = $row;
    }

    $result['aaData'] = $rows;
  } else {
    $result['aaData'] = array();
  }
}

ob_clean();
ob_start();

$result = json_encode($result);
echo $result;

ob_flush();
?>
