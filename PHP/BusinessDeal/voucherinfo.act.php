<?php

include_once 'init.php';
require_once '../../libs/connectionmanager.class.php';
require_once '../../libs/paymentmanager.class.php';
require_once '../../libs/securitymanager.class.php';
require_once '../../libs/sessionmanager.class.php';
require_once '../../libs/entitymanager.class.php';
require_once '../../libs/utils.class.php';

ob_start();

$message = "";
$status = 0;

if (SessionManager::isLoggedIn()) {

  $id = Utils::integerPost("id");  

  if ($id == NULL || $id <= 0) {
    $message = $smarty->getConfigVars("unknownId");
  } else {
    try {
      $pdo = ConnectionManager::generatePDO();
      
      $voucher_info = EntityManager::read_successful_transaction($pdo, NULL, $id);

      if ($voucher_info != null) {        
        $status = 1;
        $message = "";
      } else {
        $message = $smarty->getConfigVars("unknownProblem");        
        $status = -1;
      }
    } catch (PDOException $e) {
      $status = -1;
      $message = "Error: " + $e->getMessage();
    }
  }
  
  $result = array("result" => $status, "message" => $message);
  
  if ($voucher_info != null) {
    $count = $voucher_info[0]['count'];
    $parent_id = $voucher_info[0]['parent_id'];
    if($parent_id == 1){
      $parent_pur = $voucher_info[0]['pur_id'];
      $parent_info = PaymentManager::readPurchase($pdo, NULL, NULL, $parent_pur);
      //print_r($parent_info);
      $result['parentInfo'] = $parent_info;
    }
    if($count != NULL && $count>=0){
      $result['voucher'] = $voucher_info;
    }else{
      $count=0;
     $security_code = EntityManager::generate_voucher_password();
     $voucher_number = EntityManager::generate_voucher_number(); 
     $vouchers = EntityManager::insert_voucher($pdo, $voucher_number, $security_code, $id,$count);
     if ($vouchers != null && is_array($vouchers)) {
         $vch_id = $vouchers["id"];
         $vouchers = $vouchers['rows'];
         $voucher = array ("vch_id" => $vch_id,"count" => $count,"security_code" => $security_code,"voucher_number" => $voucher_number);
         $result['vouchers']= $voucher;
         $result['voucher'] = $voucher_info;
      }
   }
    $family = $voucher_info[0]["first_name"]." ".$voucher_info[0]["last_name"];
    $dea_id = $voucher_info[0]["dea_id"];
    $deal = EntityManager::readDeal($pdo, $dea_id);
    $title = $deal['title'];
    $result['family'] = $family;
    $result['title'] = $title;
   
  }
  
  $result = json_encode($result);
} else {
  $message = $smarty->getConfigVars("sessionExpired");
  $status = -1;
  $result = json_encode(array("result" => $status, "message" => $message, "redirect" => "/admin"));  
}

ob_clean();
ob_start();

echo $result;
ob_flush();
?>
