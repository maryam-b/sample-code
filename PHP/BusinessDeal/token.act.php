<!-- This action file handles user's token usage -->
<?php

include_once 'init.php';
require_once '../../libs/connectionmanager.class.php';
require_once '../../libs/sessionmanager.class.php';
require_once '../../libs/entitymanager.class.php';
require_once '../../libs/utils.class.php';

ob_start();

if (SessionManager::isLoggedIn()) {

  $pdo = ConnectionManager::generatePDO();

  $userId = Utils::integerGet("userId");
  $token = Utils::integerGet("token_amount");
  if ($userId > 0) {
    $now = new DateTime();
    $nowDate = $now->setTime(00, 00, 00);
    $nowDate = $now->format("Y-m-d H:i:s");
    $nowDate = $now->getTimestamp();
    $order = "end_time";
    $condition = "`usr_id` = $userId and `end_time` >= FROM_UNIXTIME(".$nowDate.") " ;
    $page = 1 ;
    $pageSize = 1;
    $tokenResult = EntityManager::selectToken($pdo, $condition, $order, $page, $pageSize);
    //print_r($tokenResult);
    if($tokenResult != null){
      
       $amount = $tokenResult[0]["amount"];
      if($amount <$token){
        $page = 2 ;
        $dec_tkn = $token - $amount;
        $decrease_tkn = EntityManager::decreaseToken($pdo, $dec_tkn, $condition, $order, $page, $pageSize);
        if($decrease_tkn != NULL){
          $tokenRes = $decrease_tkn + $amount;
        }else{
          $tokenRes = $amount;
        }       
      }else{
     
        $tokenRes = $token;
      }
     }else{
       
      $tokenRes = 0;
     }
    }
    
  } /*else {
    $states = EntityManager::selectStates($pdo);
  }*/

  $result = array();

  if ($tokenResult != null || $tokenRes == 0) {
    $result['status'] = 1;
    $result['list'] = $tokenRes;
  } else {
    $result['status'] = 0;
}

ob_clean();
ob_start();

$result = json_encode($result);
echo $result;
ob_flush();
?>
