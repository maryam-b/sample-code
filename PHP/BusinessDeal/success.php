<!-- Payment transaction by Ipay88, data from Ipay88 sent to this page and this handles payment transaction-->
<?php

    
    require_once 'libs/connectionmanager.class.php';
    require_once 'libs/entitymanager.class.php';
    require_once 'libs/sessionmanager.class.php';
    require_once 'libs/utils.class.php';
    require_once 'libs/paymentmanager.class.php';

    $pdo = ConnectionManager::generatePDO();
    $now = new DateTime();
    $show_time = date("Y-m-d");
    $respone_time = $now->getTimestamp();
    
   
   $MerchantCode = isset($_POST["MerchantCode"]) ? $_POST["MerchantCode"] : "";
   $PaymentId = isset($_POST["PaymentId"]) ? $_POST["PaymentId"] : "";
   $RefNo = isset($_POST["RefNo"]) ? $_POST["RefNo"] : 0;
   $Amount = isset($_POST["Amount"]) ? $_POST["Amount"] : "";
   $Currency = isset($_POST["Currency"]) ? $_POST["Currency"] : "MYR";
   $Remark = isset($_POST["Remark"]) ? $_POST["Remark"] : "";
   $TransId = isset($_POST["TransId"]) ? $_POST["TransId"] : "";
   $AuthCode = isset($_POST["AuthCode"]) ? $_POST["AuthCode"] : 1;
   $Status = isset($_POST["Status"]) ? $_POST["Status"] : "";  
   $ErrDesc = isset($_POST["ErrDesc"]) ? $_POST["ErrDesc"] : "";
   $Signature = isset($_POST["Signature"]) ? $_POST["Signature"] : "";
   
   $purchase = PaymentManager::readPurchase($pdo, $RefNo);
    if($purchase != NULL){
   	 $dea_id = $purchase["dea_id"];
    	 $quantity = $purchase["quantity"];
    	 $token = $purchase["token"];
	 $pur_id = $purchase["pur_id"];
         $parent_id = $purchase["parent_id"];
	 $usr_id = $purchase["usr_id"];
	 $ref_no = $purchase["ref_no"];
         $methods = $purchase["methods"];
	 $payment_method = $purchase["payment_method"];
	 $pur_amount = $purchase["amount"];
	 
	 
	$read_deal = EntityManager::readDeal($pdo, $dea_id);
    	$cur_quantity = $read_deal["current_quantity"];
    	$payment_id = PaymentManager::get_payment_id($payment_method);
    	$pur_signature = PaymentManager::createResponseSignature($payment_id, $ref_no, $pur_amount, $Status);
    	$new_quantity = $quantity + $cur_quantity;
	  	   	   
	if($Signature==$pur_signature){
	     
	 if($Status == "1"){
	       if($parent_id == 1 && $methods=="deposit" ){
                 $pur_status = 'pending';
               }else if ($parent_id == 0 && $methods=="cash"){
                 $pur_status = 'completed';
               }elseif($parent_id != 0 || $parent_id != 1){
                 $pur_status = 'completed';
               }              
	       $update_pur = PaymentManager::updatePurchase($pdo, $pur_id, $ref_no, $Status, $respone_time, $TransId,$pur_status, $ErrDesc);
               if($parent_id > 1){
                 $update_parent = EntityManager::update($pdo, "purchase", "pur", $parent_id, "pur_status", $pur_status);
               }
               if($parent_id == 0 || $parent_id == 1){
	       $update_deal = EntityManager::update($pdo, "deal", "dea", $dea_id, "current_quantity", $new_quantity);
	              if($token>0){
	              
		            $now = new DateTime();
		            $nowDate = $now->setTime(00, 00, 00);
		            $nowDate = $now->format("Y-m-d H:i:s");
		            $nowDate = $now->getTimestamp();
		            $order = "end_time";
		            $condition = "`usr_id` = $usr_id and `end_time` >= FROM_UNIXTIME(".$nowDate.") " ;
		            $page = 1 ;
		            $pageSize = 1;
		            $tokenResult = EntityManager::selectToken($pdo, $condition, $order, $page, $pageSize);
		            if($tokenResult != null){
		              $token_amount = $tokenResult[0]["amount"];
		              $tkn_id = $tokenResult[0]["tkn_id"];
		              if($token<=$token_amount){              
		                $new_amount = $token_amount - $token;
		                $update_token = EntityManager::update($pdo, "token", "tkn", $tkn_id, "amount", $new_amount);
		              }elseif($token>$token_amount){
		                $token = $token - $token_amount;
		                $new_amount = 0;
		                $update_token = EntityManager::update($pdo, "token", "tkn", $tkn_id, "amount", $new_amount);
		                $page = 2 ;
		                $tokenResult = EntityManager::selectToken($pdo, $condition, $order, $page, $pageSize);
		                if($tokenResult != NULL){
		                  $token_amount = $tokenResult[0]["amount"];
		                  $tkn_id = $tokenResult[0]["tkn_id"];
		                  $new_amount = $token_amount - $token;
		                  $update_token = EntityManager::update($pdo, "token", "tkn", $tkn_id, "amount", $new_amount);
		                }elseif($tokenResult == NULL){
		                  // create balance field for manage the expire tokens
		                }               
		              }
		            }elseif($tokenResult == NULL){
		                  // create balance field for manage the expire tokens
		            }  
       		}
         }
	        if ($update_pur != null && is_array($update_pur)) {
	          $update_pur = $update_pur['rows'];
	        }
	
	        if ($update_pur != null && $update_pur == 1) {
	         
	         $okMessage = 1;
	         
	         $infoMess = $smarty->getConfigVars("InfoMessage");
	         $infoMess = str_replace("[refNo]", $ref_no, $infoMess);
	         $smarty->assign("infoMess",str_replace("[TransId]", $TransId, $infoMess) );
	         if($parent_id == 0 || $parent_id == 1){
                    $deal_result = EntityManager::readDeal($pdo, $dea_id);
                    $userResult = EntityManager::readUser($pdo, $usr_id);
                    $profileResult = EntityManager::readProfileUser($pdo, $usr_id);
                    if($profileResult['first_name'] == NULL && $profileResult['last_name'] == NULL ){
                     $profileResult['first_name'] = $userResult['username'];
                    }
                    $save = $deal_result['original_price']-$deal_result['promotion_price'];
                    $emailAdrs = $userResult['email'];
                    $discount = $save*100/$deal_result['original_price'];
                    $digits =(is_numeric($discount) && intval($discount) == $discount ? 0 : 2);
                    $format_number = number_format($discount, $digits, '.', '');
                    $smarty->assign("discount",$format_number);
                    $smarty->assign("save",$save);
                    $smarty->assign("profileResult",$profileResult);
                    $smarty->assign("userResult",$userResult);
                    $smarty->assign("deal_result",$deal_result);
                    $smarty->assign("respone_time",$respone_time);
                    $body = $smarty->fetch("email/paymentEmail.tpl");
                    if(Utils::sendEmailFromGmail($emailAdrs, "noreply@businessdeal.my", "BusinessDeal", $object, $body)){
                            $mailMessage = $smarty->getConfigVars("receivedOrder");
                            $smarty->assign("mailMessage",$mailMessage);
                    }
                }
	       }
	       
	     }elseif($Status == "0"){
	       $pur_status = 'Failed';
	       $update_pur = PaymentManager::updatePurchase($pdo, $pur_id, $ref_no, $Status, $respone_time, $TransId,$pur_status, $ErrDesc);
	       $okMessage = 0;
	     }
	   }elseif($Signature!=$pur_signature && $Status == "1" && $pur_amount != $Amount ){
	     
	    $ErrDesc = "diffrent amount";
	    $update_pur = PaymentManager::updatePurchase($pdo, $pur_id, $ref_no, $Status, $respone_time, $TransId, $ErrDesc,$Amount);
	    $okMessage = 0;
	   }elseif($Signature!=$pur_signature){
	    	$okMessage = 0;
	   	$ErrDesc = "something wrong!Please call +60378803511 ";
	   }
    }else{
     
     $okMessage = 2;
     $wrongPaymentInfo = $smarty->getConfigVars("wrongPaymentInfo");
     $wrongPaymentInfo = str_replace("[refNo]", $RefNo, $wrongPaymentInfo);
     $smarty->assign("wrongPaymentInfo",str_replace("[TransId]", $TransId, $wrongPaymentInfo) );
   }
   $smarty->assign("okMessage", $okMessage);
   $smarty->assign("ErrDesc",$ErrDesc);
     
?>