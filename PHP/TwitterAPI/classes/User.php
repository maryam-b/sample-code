<?php

require_once("./classes/autoload.php");

Class User{

  private $user_id;
  private $name;
  private $screen_name;
  private $location;
  private $description;
  private $followers_count;
  private $friends_count;
  private $favourites_count;
  private $statuses_count;
  private $profile_image_url;

  function __construct($user=null){
    $this->mysql_cnn = new MysqlConnection;
    $this->mysql_cnn=$this->mysql_cnn->conn;
    if($user){
      $this->user_id = $user->id;
      $this->name = $user->name;
      $this->screen_name = $user->screen_name;
      $this->location = $user->location;
      $this->description = $user->description;
      $this->followers_count = $user->followers_count;
      $this->friends_count = $user->friends_count;
      $this->favourites_count = $user->favourites_count;
      $this->statuses_count = $user->statuses_count;
      $this->profile_image_url = $user->profile_image_url;
    }
  }

  public function saveUser(){
    if(!$this->isUser($this->user_id)){
      $user_id = $this->mysql_cnn->real_escape_string($this->user_id);
      $name = $this->mysql_cnn->real_escape_string($this->name);
      $screen_name = $this->mysql_cnn->real_escape_string($this->screen_name);
      $location = $this->mysql_cnn->real_escape_string($this->location);
      $description = $this->mysql_cnn->real_escape_string($this->description);
      $followers_count = $this->mysql_cnn->real_escape_string($this->followers_count);
      $friends_count = $this->mysql_cnn->real_escape_string($this->friends_count);
      $favourites_count = $this->mysql_cnn->real_escape_string($this->favourites_count);
      $statuses_count = $this->mysql_cnn->real_escape_string($this->statuses_count);
      $profile_image_url = $this->mysql_cnn->real_escape_string($this->profile_image_url);
      $query = "insert into users values ('', $user_id, '$name', '$screen_name', '$location', '$description', $followers_count,
                                          $friends_count, $favourites_count, $statuses_count, '$profile_image_url')";
      $result = $this->mysql_cnn->query($query);
      if($result)
        return true;
      else
        return false;
    }
    else
      return true;
  }

  public function showUser($user_id){
    $stmt = $this->mysql_cnn->prepare("select * from users where user_id=?");
    $stmt->bind_param('i',$user_id);
    $stmt->execute();
    $stmt->store_result();

    return $stmt;
  }

  private function isUser($user_id){
    $user_id = $this->mysql_cnn->real_escape_string($user_id);
    $result = $this->mysql_cnn->query('select id from users where user_id='.$user_id);
    $row = $result->fetch_row();
    if($row)
      return true;
    else
      return false;
  }

}