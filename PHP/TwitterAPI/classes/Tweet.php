<?php

require_once("./twitter/twitteroauth/twitteroauth.php");
require_once("./classes/autoload.php");

Class Tweet{
  // Please set your Twitter Authentication
  private $consumer_key = "";
  private $consumer_secret = "";
  private $oauth_token = "";
  private $oauth_token_secret = "";
  private $connection;
  private $mysql_cnn;

  function __construct($twitter_user= "ekomi"){
    $this->mysql_cnn = new MysqlConnection;
    $this->mysql_cnn=$this->mysql_cnn->conn;
    $this->connection = new TwitterOAuth($this->consumer_key, $this->consumer_secret, $this->oauth_token, $this->oauth_token_secret);
    $this->twitter_user = $twitter_user;
  }

  public function fetchTweets($tweets_count= 10,$tweets_page=1){
    return $tweets = $this->connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$this->twitter_user."&count=".$tweets_count."&page=".$tweets_page);

  }

  public function updateTweets($tweets){
    $tweets = json_decode(json_encode($tweets));
    foreach ($tweets as $tweet) {
      $this->user = new User($tweet->user);
      // If new user insert to database or already exists this condition should be true.
      if($this->user->saveUser()){
        $this->saveTweet($tweet);
      }
    }
  }

  public function showTweets($page=1,$count= 20){
    $stmt = $this->mysql_cnn->prepare("select * from tweets limit ?,?");
    $stmt->bind_param('ii',$start,$count);
    $strat = ($page-1)*$count;
    $stmt->execute();
    $stmt->store_result();

    return $stmt;
  }

  public function retweet($tweet_id){
    $resp=$this->connection->post("https://api.twitter.com/1.1/statuses/retweet/".$tweet_id.".json");
    if(!$resp->errors)
      $this->updateTweetField($tweet_id,"retweet_count","retweet_count+1");
  }

  public function favorite($tweet_id){
    $resp=$this->connection->post("https://api.twitter.com/1.1/favorites/create.json?id=".$tweet_id);
    if(!$resp->errors)
      $this->updateTweetField($tweet_id,"favorite_count","favorite_count+1");

  }
  public function reply($tweet_id,$text){
    $this->connection->post("https://api.twitter.com/1.1/statuses/update.json?status=".$text."&in_reply_to_status_id=".$tweet_id);
  }

  private function isTweet($tweet_id){
    $query = "select id from tweets where tweet_id=$tweet_id";
    $result = $this->mysql_cnn->query($query);
    $row = $result->fetch_row();
    if($row)
      return true;
    else
      return false;
  }

  private function saveTweet($tweet){
    $pattern = '~RT @[A-Za-z0-9_]*:~';
    if(preg_match($pattern, $tweet->text, $matches)){
      $tweet->text = str_replace($matches[0], '', $tweet->text);
      $retweet_from = str_replace('RT @', '', $matches[0]);
      $retweet_from = str_replace(':', '', $retweet_from);
    }
    if(!$this->isTweet($tweet->id)){
      // We could use PDO as well.
      $stmt = $this->mysql_cnn->prepare("insert into tweets (tweet_id, tweet_txt, in_reply_to_status_id, in_reply_to_user_id, in_reply_to_screen_name, user_id, retweet_count, favorite_count, retweet_retweet_count, retweet_favorite_count, retweet_from) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param('isiisiiiiis', $tweet_id, $tweet_txt, $in_reply_to_status_id, $in_reply_to_user_id, $in_reply_to_screen_name, $user_id, $retweet_count, $favorite_count, $retweet_retweet_count, $retweet_favorite_count, $retweet_from);
    }else{
      $stmt = $this->mysql_cnn->prepare("update tweets set tweet_txt=?, in_reply_to_status_id=?, in_reply_to_user_id=?, in_reply_to_screen_name=?, user_id=?, retweet_count=?, favorite_count=?, retweet_retweet_count=?, retweet_favorite_count=?, retweet_from=? where tweet_id=?");
      $stmt->bind_param('siisiiiiisi', $tweet_txt, $in_reply_to_status_id, $in_reply_to_user_id, $in_reply_to_screen_name, $user_id, $retweet_count, $favorite_count, $retweet_retweet_count, $retweet_favorite_count, $retweet_from, $tweet_id);
    }

    $tweet_id = $tweet->id;
    $tweet_txt = $tweet->text;
    $in_reply_to_status_id = $tweet->in_reply_to_status_id;
    $in_reply_to_user_id = $tweet->in_reply_to_user_id;
    $in_reply_to_screen_name = $tweet->in_reply_to_screen_name;
    $user_id = $tweet->user->id;
    if(isset($tweet->retweeted_status)){
      $retweet_count = $tweet->retweeted_status->retweet_count;
      $favorite_count = $tweet->retweeted_status->favorite_count;
    }

    $retweet_retweet_count = $tweet->retweet_count;
    $retweet_favorite_count = $tweet->favorite_count;

    if(!$in_reply_to_status_id)
      $in_reply_to_status_id = null;
    if(!$in_reply_to_user_id)
      $in_reply_to_user_id = null;

    $stmt->execute();
    $stmt->close();
  }


  private function updateTweetField($tweet_id,$field,$value){
    $stmt = $this->mysql_cnn->query("update tweets set $field=$value where tweet_id=$tweet_id");
    // $stmt->bind_param('ii', $value, $tweet_id);
    //  print_r($stmt);
    // $stmt->execute();

  }
}