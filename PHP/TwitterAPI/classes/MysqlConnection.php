<?php

 Class MysqlConnection{
  // Please set your database setting.
  private $db_name = '';
  private $db_user = '';
  private $db_password = '';
  private $db_host = '';

  function __construct(){
    $this->conn = new mysqli($this->db_host, $this->db_user, $this->db_password, $this->db_name) or die('MySQL Connection Problem');
  }
 }