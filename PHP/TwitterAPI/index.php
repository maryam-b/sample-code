<html>
<head>
  <link rel="stylesheet" href='./css/style.css'/>
</head>
<body>
<?php
require_once("./classes/autoload.php");

$tweet = new Tweet;
if(isset($_GET['retweet']))
	$tweet->retweet($_GET['retweet']);
if(isset($_GET['favorite']))
	$tweet->favorite($_GET['favorite']);
if(isset($_GET['reply']))
	$tweet->reply($_GET['id'],$_GET['reply']);
$tweets = $tweet->showTweets();
$tweets->bind_result($id, $tweet_id, $tweet_txt, $in_reply_to_status_id, $in_reply_to_user_id, $in_reply_to_screen_name, $user_id, $retweet_count, $favorite_count, $retweet_retweet_count, $retweet_favorite_count, $retweet_from);

while ($tweets->fetch()) {
  $user = new User;
  $user_info = $user->showUser($user_id);
  $user_info->bind_result($id, $user_id, $name, $screen_name, $location, $description, $followers_count, $friends_count, $favourites_count, $statuses_count, $profile_image_url);
  $user_info = $user_info->fetch();
?>
<center>
<div class='tweet-box'>
  <div class='Grid-cell'>
      <?php if(!$retweet_from)
        echo '<b>'.$screen_name.'</b> @'.$name.'<br>';
      else
        echo '<div class="gray small"><img src="./images/retweet.jpg" width="15px" align="center">'.$screen_name.' retweeted</div>' ?>
      <?php if(!$retweet_from)
          echo $tweet_txt;
         else
          echo '<div ><b>@'.$retweet_from.'</b><br>'.$tweet_txt.'</div>';
       ?>
    <div class="footer">
     

     <?= '<a href="index.php?retweet='.$tweet_id.'"><img src="./images/retweet2.jpg" width="20px" align="center" alt="favorite"></a>'.$retweet_count; ?>
   
        <?= '<a href="index.php?favorite='.$tweet_id.'"><img src="./images/favorite.jpg" width="15px" align="center" alt="favorite"></a>'.$favorite_count; ?>
            <div> 
            	<form method="get" >
            		<input name="id" type="hidden" value="<?=$tweet_id?>">
            		<textarea name="reply" style="width:400px"><?php if($retweet_from) echo "@".$retweet_from ?> @<?= $name?> </textarea>
            		<input  type="submit" >
            	</form>
            </div>
    </div>
  </div>
</div>
</center>
<?php
}
?>
</body>
</html>