<!-- Create Google Shortcodes for our CMS. These shortcodes make our implementer life easier they just need to add this short code

[google_conversion_script comment="Google Adwords SIGNUP Conversion Tag for Remarketing Account"
 conversion_id = "XXXX"
 conversion_format = "XXXX"
 conversion_label = "XXXX"
]

on page and that’s it.  -->

<?php
class Google extends Lib{
    /** Standard Lib Functions START **/
    function __construct()
    {
        parent::__construct();
    }

    function scopeOf($functionName)
    {
        switch ($functionName){
            case 'analytics':
                return 'endOfPage';
                break;
            case 'remarketingTrackingTag':
                return 'script';
                break;
            default:
                return '';
        }
    }
    /** Standard Lib Functions STOP **/

    /** Form Handlers START **/
    /** Form Handlers STOP **/

    /** Tags Methods START **/

    function preparsedAnalytics($params=array())
    {
        $trackerId = self::_getParam($params,'tracker_id',$this->config->google->trackingCode);
        $rollupId = self::_getParam($params,'rollup_id',$this->config->google->rollupCode);
        $domain = $this->config->site->domain;

        $ecommerceTracking = $this->_awesomeCommerceTrackingJS();

        $trackingCode = <<<HTML

        <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '$rollupId']);
        _gaq.push(['_setDomainName', '$domain']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);
        _gaq.push(['b._setAccount', '$trackerId']);  
        _gaq.push(['b._trackPageview']);
        $ecommerceTracking

          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

        </script>

HTML;
        return $trackingCode;
    }

    private function _awesomeCommerceTrackingJS(){

        $currentUri = Wrapper::getUri(true);
        list($currentPage,$queryString) = explode("?", $currentUri);

        $orderId = $_REQUEST['OrderID'] ;
        $mask = $_REQUEST['msk'] ;

        $afterSaleCookie = new Cookinator('AfterSales');
        $orderId = self::_getParam($_GET,'OrderID',$afterSaleCookie->orderNumber);
        $mask = self::_getParam($_GET,'msk',$afterSaleCookie->orderMask);

        $trackSales = false;
        foreach($this->config->google->transactionsTrackingPages as $trackingUrl){
            $urlLength = strlen($trackingUrl);
            if(strtolower(substr($currentPage,0,$urlLength)) == strtolower($trackingUrl)){
               $trackSales = true;
               break;
            }
        }

        if ($trackSales && !empty($orderId) && $afterSaleCookie->AWCTrek!=$orderId) {
            $productId = $productName = $userCountry = $quantity = '';
            if (Zend_Registry::isRegistered("order$orderId")){
                $order = Zend_Registry::get("order$orderId");
            }else{
                $order = new Api_Order($orderId);
                Zend_Registry::set("order$orderId",$order);
            }
            $totalCost=0;
            if($order->validate($mask)){
                $orderData = $order->orderData;
                $items = '';
                foreach ($orderData->order_lines as $orderLine){

                 $bundle = $orderLine->bundle;
                 $amount =sprintf("%01.2f", $orderLine->amount/100);
                 $items.=<<<ITEMS
_gaq.push(['_addItem',
                    "$orderId",                   // order ID - required
                    "$orderLine->bundle_id",                 // SKU/code
                    "$bundle->name",               // product name
                    "",                                 // category or variation
                    "$amount",                 // unit price - required
                    "$orderLine->quantity"                    // quantity - required
                ]);
ITEMS;
                $totalCost += $orderLine->amount;
                }

            }
            $totalCost =sprintf("%01.2f", $totalCost/100);
            $afterSaleCookie->AWCTrek = $orderId;//tracked flag
            return <<<JS
                _gaq.push(['_addTrans',
                    "$orderId",                   // order ID - required
                    "",                                 // affiliation or store name
                    "$totalCost",                 // total - required
                    "0",                                // tax
                    "",                                 // shipping
                    "",                                 // city
                    "",                                 // state
                    ""                // country
                ]);
                $items
                _gaq.push(['_trackTrans']);
JS;
        }
    }

    function conversionScript($params){
        $afterSaleCookie = new Cookinator('AfterSales');
        $mask = self::_getParam($_GET,'msk',$afterSaleCookie->orderMask);
        $orderNumber = self::_getParam($_GET,'OrderID',$afterSaleCookie->orderNumber) ;
        
        if($orderNumber) {

            $order = $this->getOrder($orderNumber);

            if($order->validate($mask)){
                $orderData = $order->orderData;

                $totalPaid = 0;
                $orderLine =$orderData->order_lines[$afterSaleCookie->trackedCount];
                $totalPaid += $orderLine->amount_paid;
                $lastBundle = $orderLine->bundle_id;
                $lastBundleName = $orderLine->bundle->name;

                $totalPaid = sprintf("%01.2f", $totalPaid/100);
            }
        }

        $comment = self::_getParam($params,'comment','unknown - please set');
        $conversion_id = self::_getParam($params,'conversion_id','');
        $conversion_language = self::_getParam($params,'conversion_language','en');
        $conversion_format = self::_getParam($params,'conversion_format','');
        $conversion_color = self::_getParam($params,'conversion_color','ffffff');
        $conversion_label = self::_getParam($params,'conversion_label','');
        $conversion_value = $totalPaid;

        if ($totalPaid) {
            $conversion_options = <<<EOT
            var google_conversion_value = Number($totalPaid);
EOT;
        }else{
            $conversion_options = <<<EOT
            var google_conversion_value = 0;
            var google_remarketing_only = false;
EOT;
        }

        $the_script = <<<SCRIPT

            <!-- $comment -->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = $conversion_id;
            var google_conversion_language = "$conversion_language";
            var google_conversion_format = "$conversion_format";
            var google_conversion_color = "$conversion_color";
            var google_conversion_label = "$conversion_label";
$conversion_options
            /* ]]> */
            </script>
            <div class="google_conversion_block"> 
            <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
            </script>
            </div>
            <!-- prevent conversion pixel breaking style -->
            <style>
                div.google_conversion_block, div.google_conversion_block * { position: absolute; height: 0;}
            </style>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/$conversion_id/?value=$totalPaid&amp;label=$conversion_label&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript>

SCRIPT;

      return $the_script;
    }

    private function getOrder($orderNumber){
      if (Zend_Registry::isRegistered("order$orderNumber")){
          $order = Zend_Registry::get("order$orderNumber");
      }else{
          $order = new Api_Order($orderNumber);
          Zend_Registry::set("order$orderNumber",$order);
      }
      return $order;
    }

    function remarketingTrackingTag($params){
      $comment = self::_getParam($params,'comment','unknown - please set');
      $conversion_id = self::_getParam($params,'conversion_id','');
      $conversion_label = self::_getParam($params,'conversion_label','');
      $custom_params = self::_getParam($params,'custom_params','window.google_tag_params');
      $remarketing_only = self::_getParam($params,'remarketing_only','true');

      $the_script = <<<SCRIPT

        <!-- $comment -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = $conversion_id;
        var google_conversion_label = "$conversion_label";
        var google_custom_params = $custom_params;
        var google_remarketing_only = $remarketing_only;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img  height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/$conversion_id/?value=0&amp;label=$conversion_label&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>
        <!-- prevent remarketing pixel breaking style -->
        <style style="text/css">
         iframe[name = 'google_conversion_frame']{
            height: 1px !important;
            position: absolute;
            top: 0;
         }
        </style>

SCRIPT;

      return $the_script;


    }



}
