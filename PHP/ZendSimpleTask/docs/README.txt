README
======

It’s a small application in Zend Framework (version 1.12). The application has one controller and two

views. At the first visit of the application, the “index” view shows two inputs: an email address input and 

a file upload input.

 It accepts the following comma separated csv file (header is also part of the file)

Id,Firstname,Lastname

1,Rolando,Baldwin

2,Ben,Perez

3,Ethel,Gonzales

4,Shane,Banks

The program parse the file and order it by first name. Result is something like this:

Thank you <email address that was entered on the form>. The result of the sorting is:

2, Ben, Perez
3, Ethel, Gonzales
1, Rolando, Baldwin
4, Shane, Banks
