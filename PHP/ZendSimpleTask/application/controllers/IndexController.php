<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {        
        $this->view->form = $this->getForm();
    }

    public function parsedataAction(){

      $csvUpload = new Application_Model_Uploadfile;
      $request = $this->getRequest();

      if ($this->getRequest()->isPost()) {

          $formData = $this->_request->getPost();
          $form = $this->getForm();

            if ($form->isValid($request->getPost())) {

                // success - do something with the uploaded file
                $uploadedData = $form->getValues();
                $fullFilePath = $form->file->getFileName();
                $tmp = $form->file->getFileInfo()['file']['tmp_name'];
                $csvSort = $csvUpload -> parseCsvFile($tmp);
                $this->view->result = $csvSort;
                $this->view->email = $uploadedData['email'];
                $this->render('parsedata');

            }else {

                $this->view->form=$form;
                $this->render('index');
            }
        }
    }


    protected function getForm()
    {
        $form = new Application_Form_Uploadfile();
        return $form;
    }

}



