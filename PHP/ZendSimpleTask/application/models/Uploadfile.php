<?php

class Application_Model_Uploadfile
{
  function parseCsvFile($csvFile){
    $correctPattern = null;
    $sortData=array();
    $data = fopen($csvFile,"r");
    $pattern = '/[1-9]+[0-9]*,[a-zA-Z]+,[a-zA-Z]+/';
    $headerPattern='/Id,Firstname,Lastname/';
    $header=fgets($data);
     if(preg_match($headerPattern, $header)){
        while($row=fgets($data)){
          if(preg_match($pattern, $row)){
                $tempArray=explode(",",$row);
                $sortData[$tempArray[1]]=$tempArray;
            }else{
              $correctPattern = "Your file contains line(s) that doesn't match pattern";
            }
          }            
        ksort($sortData);
      }else{
        $correctPattern = "Your file doesn't match the pattern";
      }
      $result = array ('data'=>$sortData,'errors'=>$correctPattern);
      return($result);
  }

}

