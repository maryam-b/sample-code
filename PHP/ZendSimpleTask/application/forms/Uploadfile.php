<?php

class Application_Form_Uploadfile extends Zend_Form
{

    public function init()
    {
        // Set the method for the display form to POST
        $this->setAction('/index/parsedata');
        $this->setMethod('post');
        $this->setName('upload');
        $this->setAttrib('enctype', 'multipart/form-data');
 
        // Add an email element
        $this->addElement('text', 'email', array(
            'label'      => 'Your email address:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
              array('NotEmpty', true, array('messages' => 'Please enter your Email Address!')),
              array('EmailAddress', true, array('messages' => 'Please enter a valid Email Address! ')),
            ),
            'decorators' => array(
              'ViewHelper',
              array('HtmlTag', array('tag' => 'div','class'=>'formElement')),
              array('Label'),
            ),
        ));

        // Add a file
        $this->addElement('file', 'file', array(
            'label'      => 'Please select CSV file',
            'required'   => true,
            'validators' => array('Extension'=>'csv'),
            'decorators' => array(
              'ViewHelper',
              array('HtmlTag', array('tag' => 'div','class'=>'formElement')),
              array('Label'),
            ),
        ));
 
        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Upload File',
        ));
        $this->email->removeDecorator('Errors');
        $this->file->removeDecorator('Errors');
        $this->setDecorators(array(

            'FormErrors',
            'FormElements',
            'Form',
          ));
 
    }


}

