<!-- All social media shares shortcodes, these short codes made our implementer’s life easier. they just need to add these codes on page
[facebook_share url=“PAGE_URL"]
[facebook_likebox url=“Facebook_fanpage"]
[twitter url="PAGE_URL"]
[gplus url="PAGE_URL"]
We use sharrre to have same style for all shares button
 -->

<?php
class Socialmedia extends Lib{
	
	/** Standard Lib Functions START **/
    function __construct()
    {    
        parent::__construct();

    }

    function scopeOf($functionName)
    {
        switch ($functionName){
            default:
                return '';
        }
    }
    /** Standard Lib Functions STOP **/

	
    /** Form Handlers START **/

	function requestSharrre(){
		
		header('content-type: application/json');
		$json = array('url'=>'','count'=>0);
		$json['url'] = $_GET['url'];
		$url = urlencode($_GET['url']);
		$type = urlencode($_GET['type']);
		
		if(filter_var($_GET['url'], FILTER_VALIDATE_URL)){
		  if($type == 'googlePlus'){  //source http://www.helmutgranda.com/2011/11/01/get-a-url-google-count-via-php/
		    $content = $this->parse("https://plusone.google.com/u/0/_/+1/fastbutton?url=".$url."&count=true");

		    $dom = new DOMDocument;
		    $dom->preserveWhiteSpace = false;
		    @$dom->loadHTML($content);
		    $domxpath = new DOMXPath($dom);
		    $newDom = new DOMDocument;
		    $newDom->formatOutput = true;

		    $filtered = $domxpath->query("//div[@id='aggregateCount']");
		    if (isset($filtered->item(0)->nodeValue))
		    {
		      $json['count'] = str_replace('>', '', $filtered->item(0)->nodeValue);
		    }
		  }
		  else if($type == 'stumbleupon'){
		    $content = $this->parse("http://www.stumbleupon.com/services/1.01/badge.getinfo?url=$url");

		    $result = json_decode($content);
		    if (isset($result->result->views))
		    {
		        $json['count'] = $result->result->views;
		    }

		  }
		}
		echo str_replace('\\/','/',json_encode($json));
		die();
	}


    /** Form Handlers STOP **/

    /** Tags Methods START **/

	
	
	function emailShare($params){

		$email_share = '<div class="_email"><span class="st_email_vcount" displayText="Email"></span></div>';
		if (!Zend_Registry::isRegistered('SocialMedia::emailShare')){
			$this->_injectScope('js','
				<script type="text/javascript">var switchTo5x=true;</script>
				<script type="text/javascript" src="//w.sharethis.com/button/buttons.js"></script>
				<script type="text/javascript">stLight.options({publisher: "f15b531d-4d18-4516-8a91-57e9ec8dc180", doNotHash: false, doNotCopy: false, hashAddressBar: false, onhover: false, servicePopup:true});</script>');
			Zend_Registry::set('SocialMedia::emailShare',true);
		}
		return $email_share;
		
	}
	
	function facebookRecommend($params){
		$url = Wrapper::getFullUri();
		$datahref = self::_getParam($params,'url',$url);
		$dataaction = self::_getParam($params,'type','recommend');
		$datalayout = self::_getParam($params,'layout','standard');
		$datashowfaces = self::_getParam($params,'data-show-faces','true');
		$datashare = self::_getParam($params,'data-share','true');
		$facebook_recommend = '<div class="fb-like" data-href="'.$datahref.'" data-layout="'.$datalayout.'" data-action="'.$dataaction.'" data-show-faces="'.$datashowfaces.'" data-share="'.$datashare.'" style="margin-top: 5px;"></div>';
			
			return $facebook_recommend;
	}
	
	
	function facebookComment($params){
		$url = Wrapper::getFullUri();
		$datahref = self::_getParam($params,'url',$url);
		$datanumposts = self::_getParam($params,'data-numposts','10');
		$width = self::_getParam($params,'width','520');
		$facebook_comment = '<div class="fb-comments" data-href="'.$datahref.'" data-numposts="'.$datanumposts.'" data-colorscheme="light" width="'.$width.'" ></div>';
		return $facebook_comment;
	}
	
	function facebookLikebox($params){
		$datahref = self::_getParam($params,'url');
		$datacolorscheme = self::_getParam($params,'colorscheme','light');
		$showfaces = self::_getParam($params,'showfaces','true');
		$dataheader = self::_getParam($params,'header','true');
		$showborder = self::_getParam($params,'border','true');
		$datastream = self::_getParam($params,'datastream','false');
		$width = self::_getParam($params,'width','288');
		$height = self::_getParam($params,'height','320');
		$facebook_likebox = '<div class="fb-like-box" data-href="'.$datahref.'" data-colorscheme="'.$datacolorscheme.'" data-show-faces="'.$showfaces.'" data-header="'.$dataheader.'" data-stream="'.$datastream.'" data-show-border="'.$border.'" data-width="'.$width.'" data-height="'.$height.'" style="margin-top: 5px;" ></div>';
		return $facebook_likebox;
		
	}

	function facebookShare($params){
			$url = Wrapper::getFullUri();
			$datahref = self::_getParam($params,'url',$url);
			$random = rand();
			$sharrre_facebook =<<<EOT
			<div id="facebook-$random"></div>
					<script>
					$(function(){
						$('#facebook-$random').sharrre({
						  share: {
						    facebook: true
						  },
						url : '$datahref',
						  template: '<a class="box_share" href="#"><div class="count" href="#">{total}</div><div class="share facebook_share"><span class="_facebook"></span><p class="socialtxt">Share</p></div></a>',
						  enableHover: false,
						  enableTracking: true,
						  click: function(api, options){
						    api.simulateClick();
						    api.openPopup('facebook');
						  }
						});
				});
					</script>
EOT;

			return $sharrre_facebook;
		}
	
	function twitter($params){
		$url = Wrapper::getFullUri();
		$datahref = self::_getParam($params,'url',$url);
		$random = rand();
		$sharrretwitter =<<<EOT
		<div id="twitter-$random"></div>
				<script>
				$(function(){
				$('#twitter-$random').sharrre({
				  share: {
				    twitter: true,
				  },
				url : '$datahref',
				  template: '<a class="box_share" href="#"><div class="count" href="#">{total}</div><div class="share tweet_share"><span class="_tweet"></span><p class="socialtxt">Tweet</p></div></a>',
				  enableHover: false,
				  enableTracking: true,
				  buttons: { twitter: {via: ''}},
				  click: function(api, options){
				    api.simulateClick();
				    api.openPopup('twitter');
				  }
				});
			});
				</script>
EOT;
		
		return $sharrretwitter;
	}
	
	function gplus($params){
		$url = Wrapper::getFullUri();
		$datahref = self::_getParam($params,'url',$url);
		$urlCurl = Wrapper::basePath().self::_formAction('Sharrre');
		$random = rand();
		$sharrre_gplus =<<<EOT
		<div id="googleplus-$random"></div>
				<script>
				$(function(){
					$('#googleplus-$random').sharrre({
					  share: {
					    googlePlus: true
					  },
					url : '$datahref',
					urlCurl : '$urlCurl',
					  template: '<a class="box_share" href="#"><div class="count" href="#">{total}</div><div class="share gplus_share"><span class="_gplus"></span><p class="socialtxt">Share</p></div></a>',
					  enableHover: false,
					  enableTracking: true,
					  click: function(api, options){
					    api.simulateClick();
					    api.openPopup('googlePlus');
					  }
					});
			});
				</script>
EOT;
		
		return $sharrre_gplus;
	}
	
		
		function stumbleupon($params){
			$url = Wrapper::getFullUri();
			$datahref = self::_getParam($params,'url',$url);
			$urlCurl = Wrapper::basePath().self::_formAction('Sharrre');
			$random = rand();
			$sharrre_stumbleupon =<<<EOT
			<div id="stumbleupon-$random"></div>
					<script>
					$(function(){
						$('#stumbleupon-$random').sharrre({
						  share: {
						    stumbleupon: true
						  },
						url : '$datahref',
						urlCurl : '$urlCurl',
						  template: '<a class="box_share" href="#"><div class="count" href="#">{total}</div><div class="share stumble_share"><span class="_stumble"></span><p class="socialtxt">Share</p></div></a>',
						  enableHover: false,
						  enableTracking: true,
						  click: function(api, options){
						    api.simulateClick();
						    api.openPopup('stumbleupon');
						  }
						});
				});
					</script>
EOT;

			return $sharrre_stumbleupon;
		}

		function pinterest($params){
		$url = Wrapper::getFullUri();
		$datahref = self::_getParam($params,'url',$url);
		$random = rand();
		$sharrretwitter =<<<EOT
		<div id="pinterest-$random"></div>
				<script>
				$(function(){
				$('#pinterest-$random').sharrre({
				  share: {
				    pinterest: true,
				  },
				url : '$datahref',
				  template: '<a class="box_share" href="#"><div class="count" href="#">{total}</div><div class="share pinterest_share"><span class="_pinterest"></span><p class="socialtxt">Pin it</p></div></a>',
				  enableHover: false,
				  enableTracking: true,
				  buttons: { pinterest: {via: ''}},
				  click: function(api, options){
				    api.simulateClick();
				    api.openPopup('pinterest');
				  }
				});
			});
				</script>
EOT;
		
		return $sharrretwitter;
	}

		private function parse($encUrl){
		    $options = array(
		      CURLOPT_RETURNTRANSFER => true, // return web page
		      CURLOPT_HEADER => false, // don't return headers
		      CURLOPT_FOLLOWLOCATION => true, // follow redirects
		      CURLOPT_ENCODING => "", // handle all encodings
		      CURLOPT_USERAGENT => 'sharrre', // who am i
		      CURLOPT_AUTOREFERER => true, // set referer on redirect
		      CURLOPT_CONNECTTIMEOUT => 5, // timeout on connect
		      CURLOPT_TIMEOUT => 10, // timeout on response
		      CURLOPT_MAXREDIRS => 3, // stop after 10 redirects
		      CURLOPT_SSL_VERIFYHOST => 0,
		      CURLOPT_SSL_VERIFYPEER => false,
		    );
		    $ch = curl_init();

		    $options[CURLOPT_URL] = $encUrl;  
		    curl_setopt_array($ch, $options);

		    $content = curl_exec($ch);
		    $err = curl_errno($ch);
		    $errmsg = curl_error($ch);

		    curl_close($ch);

		    if ($errmsg != '' || $err != '') {
		      /*print_r($errmsg);
		      print_r($errmsg);*/
		    }
		    return $content;
		  }	
}
    /** Tags Methods END **/	
?>