/* would better to create an object of google map to create dynamic marker. */
var googleMap={
  showMap:function(position){
    lat=position.coords.latitude;
    long=position.coords.longitude;
    document.getElementById('lat').value=lat;
    document.getElementById('long').value=long;
    my_location=new google.maps.LatLng(lat,long);
    var mapProp = {
      center:my_location,
      zoom:12,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    googleMap.map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    new google.maps.Marker({
      position:my_location,
      map:googleMap.map,
      icon:"images/my_location.png",
      title: 'Your are here!',
    });

  },
  addMarker:function(lat,long,location,uber_estimate){
      newloc=new google.maps.LatLng(lat,long);
      console.log(googleMap.test);
      new google.maps.Marker({
          position:newloc,
          map:googleMap.map,
          icon:"images/locations.png",
        });

  },


};
  navigator.geolocation.getCurrentPosition(googleMap.showMap);
