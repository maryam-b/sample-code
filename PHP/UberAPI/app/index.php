<!DOCTYPE html>
<html>
<head>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css">
<!-- <script src="js/map.js"></script>-->
</head>
<body>
  <script type="text/javascript">
  navigator.geolocation.getCurrentPosition(showMap);
function showMap(position){

    lat=position.coords.latitude;
    long=position.coords.longitude;
    document.getElementById('lat').value=lat;
    document.getElementById('long').value=long;
    my_location=new google.maps.LatLng(lat,long);
    var mapProp = {
      center:my_location,
      zoom:12,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    new google.maps.Marker({
      position:my_location,
      map:map,
      icon:"images/location.png",
      title: 'Your are here!',
    });
  <?php
  $error=null;
  if(isset($_GET['action']) and $_GET['action']=='search'){
    if(isset($_GET['lat'],$_GET['long'])){
      $url='*****?lat='.$_GET['lat'].'&long='.$_GET['long'].'&q='.$_GET['query'];
      $tours=json_decode(file_get_contents($url));

      foreach($tours as $tour){ // add marker for each tour
        ?>
        tour_location=new google.maps.LatLng(<?=$tour->coordinates->lat?>,<?=$tour->coordinates->long?>);

        marker<?=$tour->tour_id?>=new google.maps.Marker({
        position:tour_location,
          map:map,
          icon:"images/location.png",
          title: '<?=$tour->title?>
          test',
        });

        var tour_desc<?=$tour->tour_id?> = '<h3><?=$tour->title?></h3>'+
         '<h4>Uber estimate prices from your location:</h4>'+
         '<?php foreach($tour->uber as $uber){?>'+
         '<b><?=$uber->localized_display_name?></b>: <?=$uber->estimate?><br/>'+
         '<?php }?>';

        var infowindow<?=$tour->tour_id?> = new google.maps.InfoWindow({
          content: tour_desc<?=$tour->tour_id?>
        });
        google.maps.event.addListener(marker<?=$tour->tour_id?>, 'click', function() {
          infowindow<?=$tour->tour_id?>.open(map,marker<?=$tour->tour_id?>);
        });
  <?php
      }//end foreach
    }else{
      $error="cannot find your location! Please try again :) ";
    }//end if
  }//end if
  ?>
}//end showMap function
</script>
<div id="googleMap" style="width:1000px;height:500px;"></div>
<form method='get' action='index.php' class='formdata'>
  <input type='hidden' name='lat' id='lat' />
  <input type='hidden' name='long' id='long' />
  <input type='text' name='query' value='<?= isset($_GET['query']) ? $_GET['query'] : "" ?>' class='search' />
  <input name='action' type='submit' value='search'/>
</form>

<div class='error'>
  <?php
  if($error)
    echo $error;
  ?>
</div>
</body>
</html>