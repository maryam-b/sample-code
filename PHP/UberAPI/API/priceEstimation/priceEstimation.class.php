<?php
class priceEstimation{
  private $api_name='Price Estimation';
  private $api_ver='1.0.0';
  private $status;
  private $currency='EUR'; // Default currency

  CONST LANGUAGE='en'; // It could set based on user's setting
  CONST LIMIT= 10;



  public function badRequest(){
    $response=$this->setResponse("error");
    $response["message"]="Api request structure doesn't match";
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  public function response($query,$lat,$long,$currency){
    if (!is_null($currency))
      $this->currency = $currency;
    $tours = $this->tourResponse($query);
    foreach ($tours as $tour) {
      $uber_data=$this->uberResponse($lat,$long,$tour->coordinates->lat,$tour->coordinates->long);
      if($uber_data){
        $tour->uber=$uber_data->prices;
      }
    }
    header('Content-Type: application/json');
    echo json_encode($tours);
  }


  private function setResponse($status){
    return(array("API Name"=>$this->api_name,"API Version"=>$this->api_ver,"status"=>$status));
  }

  private function tourResponse($query){
    $base_url = '****';
    $access_token = '***';
    $url = $base_url.'?access_token='.$access_token.'&currency='.$this->currency.'&cnt_language='.self::LANGUAGE.'&q='.$query.'&limit='.self::LIMIT;

    return json_decode(file_get_contents($url))->data->tours; // The reason that I used file_get_content is it doesn't need any http php modules to be installed

  }

  private function uberResponse($start_latitude,$start_longitude,$end_latitude,$end_longitude){
    $base_url = "***";
    $server_token = '****';
    $url = $base_url.'?server_token='.$server_token.'&start_latitude='.$start_latitude.'&start_longitude='.$start_longitude.'&end_latitude='.$end_latitude.'&end_longitude='.$end_longitude;
    if($uber_res=@file_get_contents($url))
      return json_decode($uber_res);
    else
      return false;
  }
}
?>